<!DOCTYPE html>
<!--[if gt IE 8]> <html class="ie9" lang="en"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml" class="ihome">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <title>SVRI</title>
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700,300" rel="stylesheet" type="text/css" />
  <link href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic" rel="stylesheet" type="text/css" />
  <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
  <link href="css/animate.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/green.css" id="style-switch" />
  <!-- REVOLUTION BANNER CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.min.css" media="screen" />
  <link rel="icon" type="image/png" href="images/LOGO.svg">
  <!--[if IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie9.css" />    <![endif]-->
  <link rel="stylesheet" type="text/css" href="css/slides.css" />
  <link rel="stylesheet" type="text/css" href="css/inline.min.css" />
</head>

<body>
  <div id="loader-overlay"><img alt="Loading" src="images/loader.gif" /></div>
  
  <?php include 'menuPrincipal.html'; ?>

  <div class="complete-content">
    <div class="container full-width-container ihome-banner">
      <div class="banner col-sm-12 col-xs-12 col-md-12">
        <ul>
          <!-- THE BOXSLIDE EFFECT EXAMPLES  WITH LINK ON THE MAIN SLIDE EXAMPLE -->
          <li data-transition="papercut" data-slotamount="3"> <img src="images/new-slider/s1-bg.jpg" />
            <div class="caption lfb boxshadow" data-x="70" data-y="120" data-speed="900" data-start="500" data-easing="easeOutBack" data-autplay="true">
              <!-- <iframe src="videos/intro.mp4" width="460" -->
              <iframe width="470" height="355" frameborder="10" src="images/intro/VIDEO_SVRI.mp4" allowfullscreen=""></iframe>
            </div>

            <div class="tp-caption lft skewtoright imed-sl1" data-x="600" data-y="80" data-hoffset="61" data-speed="1600" data-start="1000" data-easing="Power4.easeOut" data-endspeed="400" data-endeasing="Power1.easeIn">
              <img src="images/intro/bienvenidos.png" alt="" class="img-responsive" />
            </div>
            
            <div class="tp-caption lft skewtoright imed-sl1" data-x="700" data-y="250" data-hoffset="61" data-speed="1600" data-start="1900" data-easing="Power4.easeOut" data-endspeed="400" data-endeasing="Power1.easeIn">
              <img src="images/intro/sociedad.png" alt="" class="img-responsive" />
            </div>
          </li>

          <li data-transition="papercut" data-slotamount="7"> <img src="images/new-slider/s1-bg.jpg" />

            <div class="tp-caption lft skewtoright imed-sl1" data-x="-20" data-y="30" data-hoffset="61" data-speed="1600" data-start="2000" data-easing="Power4.easeOut" data-endspeed="400" data-endeasing="Power1.easeIn">
              <img src="images/avisos/XXV_anos.png" alt="" class="img-responsive" style="width:100%" />
            </div>
            &lt;

          </li>
          
          <div style="display: none">
            <li data-transition="papercut" data-slotamount="7"> <img src="images/new-slider/s1-bg.jpg" />

              <div class="tp-caption lft skewtoright imed-sl1" data-x="-20" data-y="30" data-hoffset="61" data-speed="1600" data-start="2000" data-easing="Power4.easeOut" data-endspeed="400" data-endeasing="Power1.easeIn">
                <img src="images/avisos/aviso_1.png" alt="" class="img-responsive" style="width:100%" />
              </div>
              &lt;

            </li>
          </div>

        </ul>
      </div>
    </div>
  
          <div class="col-sm-12 col-xs-12 col-md-12" style="padding-top: 100px">
        <embed src="documentos/Anualidad_2017_Activos_Enero.pdf" type="application/pdf" width="100%" height="500px">
      </div>
  </div>
  <div class="complete-footer">
    <div class="bottom-footer">
      <div class="container">
        <div class="row">
          <!--Foot widget-->
          <div class="col-xs-12 col-sm-12 col-md-12 foot-widget-bottom">
            <p class="col-xs-12 col-md-5 no-pad">MAGEST Software 2015 | All Rights Reserved</p>
            <ul class="foot-menu col-xs-12 col-md-7 no-pad">
              <li><a href="contacto.html">Contacto</a></li>
              <li><a href="links_rad.html">Links radiológicos</a></li>
              <li><a href="verimagenes.html">VerImagenes</a></li>
              <li><a href="publico_gral.html">Público en general</a></li>
              <li><a href="quienes_somos.html">¿Quiénes somos?</a></li>
              <li><a href="index.html">Inicio</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--JS Inclution-->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
  <script type="text/javascript" src="bootstrap-new/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
  <script type="text/javascript" src="js/jquery.scrollUp.min.js"></script>
  <script type="text/javascript" src="js/jquery.sticky.min.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/jquery.flexisel.min.js"></script>
  <script type="text/javascript" src="js/jquery.imedica.min.js"></script>
  <script type="text/javascript" src="js/custom-imedicajs.min.js"></script>
  <script type="text/javascript">
    $(window).load(function() {
      $('#loader-overlay').fadeOut(900);
      $("html").css("overflow", "visible");
    });
  </script>
</body>

</html>
