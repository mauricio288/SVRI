<!DOCTYPE HTML>
<!--[if gt IE 8]> <html class="ie9" lang="en"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


  <title>SVRI</title>

  <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
  <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
  <link href="css/animate.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/green.css" id="style-switch" />

  <!-- REVOLUTION BANNER CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

  <!--[if IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie9.css" />
    <![endif]-->

  <link rel="icon" type="image/png" href="images/LOGO.svg">
  <link rel="stylesheet" type="text/css" href="css/inline.min.css" />
</head>

<body>

<?php include 'menuPrincipal.html'; ?>

  <section class="complete-content content-footer-space">

    <div class="about-intro-wrap pull-left">

      <div class="bread-crumb-wrap ibc-wrap-1">
        <div class="container">
          <!--Title / Beadcrumb-->
          <div class="inner-page-title-wrap col-xs-12 col-md-12 col-sm-12">
            <div class="bread-heading">
              <h1>Ultrasonido</h1></div>
            <div class="bread-crumb pull-right">
              <ul>
                <li><a href="index.html">Inicio</a></li>
                <li><a href="publico_gral.html">Público en general</a></li>
                <li><a href="ultrasonido.html">Ultrasonido</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="container">

        <div class="row">

          <!--About-us top-content-->

          <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 column-element">

            <h3>Ultrasonido</h3>
            <p>
              Aunque los tejidos del cuerpo son opacos a la luz, el cuerpo humano es relativamente transparente a otras formas de radiaciones como los rayos-X, las partículas nucleares y las ondas ultrasonográficas que permiten explorar los diferentes órganos y la
              detección de múltiples enfermedades.</p>
            <p>
              El ultrasonido ha tenido una gran aceptación universal debido a que permite ver imágenes en tiempo real (en movimiento), es un método de diagnóstico económico en comparación con otros y no tiene radiación ionizante, sus ondas de naturaleza mecánica o
              vibraciones sonoras utilizan una frecuencia superior al limite de la audición humana ( 20.000 ciclos /seg.) no la podemos oír debido a que representa una escala por arriba de 10Hz (ultrasónica) menos de 16Hz se le conoce como infrasónica,
              dicho fenómeno físico parte de las vibraciones emitidas por un transductor que el médico tiene en su mano colocado sobre el paciente y un gel que permite la mejor trasmisión sobre la superficie de la piel, las ondas emisoras y receptoras
              se propagan dentro del cuerpo chocando con las diferentes texturas orgánicas y emitiendo hacia el equipo una señal electrónica que se convierte en imágenes con una amplia escala de grises, actualmente una modalidad en color se conoce como
              dopplery selectivamente se usa para medir las velocidades de la sangre.
            </p>
            <p>
              Existen referencias bibliográficas desde 1915 de los fenómenos magnetostáticos y piezoeléctricos que ya sugerían estas propiedades conductivas, sin embargo se dice que en 1880 Pierre Curie ya describía la facultad de ciertos cristales como el cuarzo,
              la turmalina, el clorato potásico, la sal gema, la blenda y otros elementos naturalmente conductores, es evidente que en la actualidad los transductores tienen una configuración compleja de cristales para favorecer una imagen de alta definición.
            </p>
            <p>
              Recordemos el sonar o radar de un barco utilizado en la guerra para identificar la posición estratégica del enemigo, minas o submarinos en el fondo del mar, en donde la onda emisora choca con el sólido y regresa como onda receptora al control del barco,
              por este mecanismo se puede calcular la distancia y el tamaño del objeto, en medicina es similar traducido en imágenes anatómicas.
            </p>
            <p>
              El agua es el mejor conductor de la onda ultrasónica y el aire el peor enemigo por tal motivo los órganos con liquido serán mejor vistos, ejemplo; la vejiga, los quistes ováricos, la vesícula biliar etc, los tejidos semiblandos son buenos conductores
              estables como el hï¿½gado, el bazo y el útero, los tejidos muy sólidos se pueden ver pero producen sombras posteriores al choque de la onda emisora por ejemplo el hueso, en el caso del aire sólo vemos altas densidades
            </p>
            <p>
              Existen muchas aplicaciones del ultrasonido en medicina sin embargo las mas frecuentes son en la obstetricia ( placenta, feto, partes maternas ), la ginecología ( útero y ovarios) gastroenterología ( hígado, vesícula, vías biliares ) urología ( riñones,
              vejiga y próstata )
            </p>
            <p>
              Pueden detectarse múltiples e incontables enfermedades ya que podemos usarlo a través de la cabeza únicamente en recién nacidos para conocer su cerebro, en los ojos para demostrar cataratas, en los senos paranasales de niños para sinusitis exudativa,
              en el cuello para la tiroides o tumores, en el corazón para demostrar cardiopatías congénitas en bebes y adquiridas en adultos, casi todos los órganos del abdomen pueden verse, actualmente en los servicios de urgencias se detectan apendicitis
              en fase temprana, piedras en vesícula, riñones, conductos, tumores en páncreas, hígado, pelvis, enfermedades en la matriz y los ovarios de la mujer, defectos congénitos en el embrión y el feto, etc.
            </p>
            <p>
              Debemos reconocer que es el único método en donde el resultado diagnóstico y la calidad del estudio depende del explorador en mas del 80% de los casos, es muy importante que el responsable del estudio tenga pleno conocimiento de las rutinas, la física
              del método y la problemática del paciente para lograr integrar una adecuada exploración 100% intencionada desde el punto de vista clínico-ecográfico, los falsos positivos obedecen frecuentemente a falta de experiencia del médico o ausencia
              de un protocolo exploratorio establecido.
            </p>
            <p>
              Las modalidades del método integran actualmente el doppler color para valoración de las arterias y venas, el ultrasonido tridimensional que representa la imagen estática mas que diagnóstica y los equipos de banda ancha que permiten optimizar imágenes
              armónicas con o sin ecorealzadores (burbujas inyectadas por las venas que mejoran la imagen).
            </p>
            <p>
              Dr. F. Grosskelwing Acosta.
            </p>





          </div>


        </div>
      </div>


    </div>

  </section>



  <section class="complete-footer">
    <div class="bottom-footer">
      <div class="container">

        <div class="row">
          <!--Foot widget-->
          <div class="col-xs-12 col-sm-12 col-md-12 foot-widget-bottom">
            <p class="col-xs-12 col-md-5 no-pad">MAGEST Software 2015 | All Rights Reserved</p>
            <ul class="foot-menu col-xs-12 col-md-7 no-pad">

              <li><a href="contacto.php">Contacto</a></li>
            <li><a href="links_rad.php">Links radiológicos</a></li>
            <li><a  href="verimagenes.php">VerImagenes</a></li>
            <li><a href="publico_gral.php">Público en general</a></li>
            <li><a href="quienes_somos.php">¿Quiénes somos?</a></li>
            <li><a href="index.php">Inicio</a></li>


            </ul>
          </div>
        </div>
      </div>
    </div>

  </section>

  <!--JS Inclution-->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
  <script type="text/javascript" src="bootstrap-new/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
  <script type="text/javascript" src="js/jquery.scrollUp.min.js"></script>
  <script type="text/javascript" src="js/jquery.sticky.min.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/jquery.flexisel.min.js"></script>
  <script type="text/javascript" src="js/jquery.imedica.min.js"></script>
  <script type="text/javascript" src="js/custom-imedicajs.min.js"></script>

</body>

</html>
