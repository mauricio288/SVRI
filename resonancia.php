<!DOCTYPE HTML>
<!--[if gt IE 8]> <html class="ie9" lang="en"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


  <title>SVRI</title>

  <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
  <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
  <link href="css/animate.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/green.css" id="style-switch" />

  <!-- REVOLUTION BANNER CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

  <!--[if IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie9.css" />
    <![endif]-->

  <link rel="icon" type="image/png" href="images/LOGO.svg">
  <link rel="stylesheet" type="text/css" href="css/inline.min.css" />
</head>

<body>

<?php include 'menuPrincipal.html'; ?>

  <section class="complete-content content-footer-space">

    <div class="about-intro-wrap pull-left">

      <div class="bread-crumb-wrap ibc-wrap-1">
        <div class="container">
          <!--Title / Beadcrumb-->
          <div class="inner-page-title-wrap col-xs-12 col-md-12 col-sm-12">
            <div class="bread-heading">
              <h1>Resonancia magnética</h1></div>
            <div class="bread-crumb pull-right">
              <ul>
                <li><a href="index.html">Inicio</a></li>
                <li><a href="publico_gral.html">Público en general</a></li>
                <li><a href="resonancia.html">Resonancia magnética</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="container">

        <div class="row">

          <!--About-us top-content-->

          <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 column-element">
            <h3>Resonancia Magnética</h3>
            <p>
              Para entender la resonancia magnética es necesario conocer ciertos principios básicos:
            </p>

            <h4>Magnetismo:</h4>
            <p>
              En el año 2600 A.C. en China se descubre que ciertas piezas de metal desplazan a otras sin tocarlas, en el siglo 13 Pierre de Mericourt describe las propiedades del imá describiendo dos polos: norte (-) y sur (+), los polos de signo inverso se atraen
              y los opuestos se rechazan, alrededor de ambos polos de los imanes existe un campo magnético. Este campo no es uniforme, pero si se curva un imán se vuelve uniforme.
            </p>
            <p>
              Para la resonancia magnética (R.M.) es necesario campos magnéticos muy grandes, el campo magnético de la tierra es de 0.05 gauss, en la R.M. los campos varían de 400 a 20,000 gauss (10,000 gauss = 1 tesla). Los campos magnéticos tiene una óntima relación
              con la corriente eléctrica, por lo que mediante electricidad es posible crear electroimanes muy poderosos para ser usados en R.M.
            </p>

            <h4>Átomo:</h4>
            <p>
              Pitágoras bautizo al constituyente principal de la materia como á al comienzo del siglo XIX el químico Dalton prueba la existencia del átomo y lo representa como una esfera compacta cuya masa varía de un elemento a otro. En el siglo XX Rutherford lo describe
              como un núcleo central cargado positivamente rodeado de electrones cargados negativamente. El comportamiento del núcleo es lo que se modifica en los estudios de R.M.
            </p>
            <p>
              Como es bien sabido el núcleo esta compuesto de protones y neutrones; los neutrones son eléctricamente neutros mientras que los protones tienen cargas positivas, a causa de sus cargas el núcleo se comporta como dipolo, motivo por el cual se crea un campo
              eléctrico mismo que se orienta en una dirección que al ponerse en un campo magnético se alinea en dirección a este campo como lo haría una brújula.
            </p>
            <p>
              Los núcleos al igual que los electrones giran sobre si mismos a gran velocidad fenómeno que en ingles se denomina "spin" ( to spin ). Por lo que se dirá que los spines se alinean en dirección del campo magnético. El elemento más simple de los núcleos
              es el del hidrógeno, compuesto por un solo protón , mismo que existe de manera abundante en el cuerpo humano y es el único que se emplea para la adquisición de imágenes por R.M.
            </p>
            <p>
              Los átomos de hidrógeno al ser expuestos a un campo magnético se alinean de dos maneras: hacia el norte antiparalelo y hacia el sur paralelo. Entre las dos posiciones existe una diferencia de energía lo que va a provocar la detección de una señal de R.M.
            </p>

            <h4>Resonancia:</h4>
            <p>
              La resonancia es la transferencia de energía entre dos sistemas que oscilan a la misma frecuencia, es decir, un objeto es puesto en resonancia cuando es expuesto a una onda cuya frecuencia corresponde a su propia frecuencia. En R. M. las ondas utilizadas
              son de 1 a 100 Mega hertz, las mismas que se utilizan como ondas de radio, de esta manera cuando los protones entran en resonancia al ser expuestos a una onda de su misma frecuencia, cambian de posición con respecto a la posición de sus
              ejes liberando energía.
            </p>

            <h4>De la señal a la imagen:</h4>
            <p>En una primera etapa se define un numero de puntos a los largo de un eje, ya sea sagital, coronal o axial, cada punto resonando a una determinada frecuencia propia y ligeramente diferente a sus puntos vecinos, se repiten el procedimiento las
              veces que se consideren necesarias hasta completar el volumen total a explorar, todas las señales adquiridas se procesan matemáticamente mediante la computadora otorgándole un color en escala de grises de acuerdo al tiempo en que se genere
              la señal o el tiempo que tardan los protones en recuperar la energía perdida en su spin. </p>




          </div>


        </div>
      </div>


    </div>

  </section>




  <section class="complete-footer">

    <div class="bottom-footer">
      <div class="container">

        <div class="row">
          <!--Foot widget-->
          <div class="col-xs-12 col-sm-12 col-md-12 foot-widget-bottom">
            <p class="col-xs-12 col-md-5 no-pad">MAGEST Software 2015 | All Rights Reserved</p>
            <ul class="foot-menu col-xs-12 col-md-7 no-pad">

             <li><a href="contacto.php">Contacto</a></li>
            <li><a href="links_rad.php">Links radiológicos</a></li>
            <li><a  href="verimagenes.php">VerImagenes</a></li>
            <li><a href="publico_gral.php">Público en general</a></li>
            <li><a href="quienes_somos.php">¿Quiénes somos?</a></li>
            <li><a href="index.php">Inicio</a></li>


            </ul>
          </div>
        </div>
      </div>
    </div>

  </section>

  <!--JS Inclution-->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
  <script type="text/javascript" src="bootstrap-new/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
  <script type="text/javascript" src="js/jquery.scrollUp.min.js"></script>
  <script type="text/javascript" src="js/jquery.sticky.min.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/jquery.flexisel.min.js"></script>
  <script type="text/javascript" src="js/jquery.imedica.min.js"></script>
  <script type="text/javascript" src="js/custom-imedicajs.min.js"></script>

</body>

</html>
