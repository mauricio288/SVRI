<!DOCTYPE html>
<!--[if gt IE 8]> <html class="ie9" lang="en"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport" />
    <title>SVRI</title>
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic" />
    <link rel="stylesheet" href="css/jquery-ui-1.10.3.custom.css" />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link id="style-switch" href="css/green.css" type="text/css" rel="stylesheet" />
    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link media="screen" href="rs-plugin/css/settings.css" type="text/css" rel="stylesheet" />
    <!--[if IE 9]>        <link rel="stylesheet" type="text/css" href="css/ie9.css" />    <![endif]-->
    <link href="images/SVRI.jpg" type="image/png" rel="icon" />
    <link href="css/inline.min.css" type="text/css" rel="stylesheet" /> </head>

<body>

    <?php include 'menuPrincipal.html'; ?>

    <!-- STYLE="display: none"= NO SE VE NADA, STYLE="display: yes" = SE MUESTRA-->
    <section class="complete-content content-footer-space" style="display: yes">
        <div class="about-intro-wrap pull-left">
            <div class="bread-crumb-wrap ibc-wrap-1">
                <div class="container">
                    <!--Title / Beadcrumb-->
                    <div class="inner-page-title-wrap col-xs-12 col-md-12 col-sm-12">
                        <div class="bread-heading">
                            <h1>VerImagenes</h1> </div>
                        <div class="bread-crumb pull-right">
                            <ul>
                                <li><a href="index.php">Inicio</a></li>
                                <li><a href="verimagenes.php">VerImagenes</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 column-element">
                        <center> <img class="img-responsive" src="images/avisos/portadaVer_2017.jpg" /> </center>
                        <center>
                            <br />
                            <table width="1019" border="0" align="center">
                                <tbody>
                                    <tr>
                                        <td width="339">
                                            <div align="center">
                                                <p> <b>PROGRAMA CIENTIFICO</b> </p>
                                               
                                                <button class="btn btn-primary"> <a style="color:#fff" target="_blank" href="documentos/programa_cientifico.pdf">PDF</a> </button>
                                                <button  class="btn btn-primary"> <a style="color:#fff" target="_blank" href="documentos/programa_cientifico.jpg">JPG</a> </button>
                                            </div>
                                        </td>
                                        <td width="313">
                                            <div align="center">
                                                <p> <b>PROGRAMA COMPLETO</b> </p>
                                                
                                                <button  class="btn btn-primary"> <a style="color:#fff" target="_blank" href="documentos/PROGR_2017.pdf">PDF</a> </button>
                                                
                                            </div>
                                        </td>
                                        <td width="337">
                                            <div align="center">
                                                <p> <b>FORMATO DE REGISTRO</b> </p>
                                                <button class="btn btn-primary"> <a style="color:#fff" target="_blank" href="documentos/registro_2017.pdf">PDF</a> </button>
                                                <button class="btn btn-primary"> <a style="color:#fff" target="_blank" href="images/avisos/costosVer_2017.jpg">JPG</a> </button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </center>
                        <br />
                        <br />
                        <center> </center>
                        <br />
                        
                            <br />
                            <br /> </div>
                        <br /> </div>
                </div>
            </div>
        </div>
        <div class="cl-wrap icl-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pull-left client-logo-flex wow flipInX" data-wow-delay="0.5s" data-wow-offset="100">
                        <ul id="testi-client-logos" class="icl-carousel">
                            <li>
                                <a href=""><img alt="" src="images/verimagenes/1997.jpg" class="img-responsive client-logo-img" /></a>
                            </li>
                            <li>
                                <a href=""><img alt="" src="images/verimagenes/1998.jpg" class="img-responsive client-logo-img" /></a>
                            </li>
                            <li>
                                <a href=""><img alt="" src="images/verimagenes/1999.jpg" class="img-responsive client-logo-img" /></a>
                            </li>
                            <li>
                                <a href=""><img alt="" src="images/verimagenes/2000.jpg" class="img-responsive client-logo-img" /></a>
                            </li>
                            <li>
                                <a href=""><img alt="" src="images/verimagenes/2001.jpg" class="img-responsive client-logo-img" /></a>
                            </li>
                            <li>
                                <a href=""><img alt="" src="images/verimagenes/2002.jpg" class="img-responsive client-logo-img" /></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="complete-footer">
        <div class="bottom-footer">
            <div class="container">
                <div class="row">
                    <!--Foot widget-->
                    <div class="col-xs-12 col-sm-12 col-md-12 foot-widget-bottom">
                        <p class="col-xs-12 col-md-5 no-pad">MAGEST Software 2015 | All Rights Reserved</p>
                        <ul class="foot-menu col-xs-12 col-md-7 no-pad">
                            <li><a href="contacto.php">Contacto</a></li>
                            <li><a href="links_rad.php">Links radiológicos</a></li>
                            <li><a href="verimagenes.php">VerImagenes</a></li>
                            <li><a href="publico_gral.php">Público en general</a></li>
                            <li><a href="quienes_somos.php">¿Quiénes somos?</a></li>
                            <li><a href="index.php">Inicio</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--JS Inclution-->
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script src="bootstrap-new/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="rs-plugin/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
    <script src="rs-plugin/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
    <script src="js/jquery.scrollUp.min.js" type="text/javascript"></script>
    <script src="js/jquery.sticky.min.js" type="text/javascript"></script>
    <script src="js/wow.min.js" type="text/javascript"></script>
    <script src="js/jquery.flexisel.min.js" type="text/javascript"></script>
    <script src="js/jquery.imedica.min.js" type="text/javascript"></script>
    <script src="js/custom-imedicajs.min.js" type="text/javascript"></script>
</body>

</html>
