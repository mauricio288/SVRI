<!DOCTYPE HTML>
<!--[if gt IE 8]> <html class="ie9" lang="en"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


  <title>SVRI</title>

  <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
  <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
  <link href="css/animate.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/green.css" id="style-switch" />

  <!-- REVOLUTION BANNER CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

  <!--[if IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie9.css" />
    <![endif]-->

  <link rel="icon" type="image/png" href="images/LOGO.svg">
  <link rel="stylesheet" type="text/css" href="css/inline.min.css" />
</head>

<body>

    <?php include 'menuPrincipal.html'; ?>
  

  <section class="complete-content content-footer-space">

    <!--Mid Content Start-->

    <div class="about-intro-wrap pull-left">

      <div class="bread-crumb-wrap ibc-wrap-5">
        <div class="container">
          <!--Title / Beadcrumb-->
          <div class="inner-page-title-wrap col-xs-12 col-md-12 col-sm-12">
            <div class="bread-heading">
              <h1>Contacto</div>
                <div class="bread-crumb pull-right">
                <ul>
                <li><a href="index.html">Inicio</a></li>
                <li><a href="contacto.html">Contacto </a></li>
                </ul>
                </div>
            </div>
         </div>
     </div>

     <!--map-->
            	<div class="pull-left map-full no-pad">
                	<div id="map-canvas-2"></div>
                	<div class="map-shadow"></div>
                </div>

         <div class="container">



            <!--About-us top-content-->

        	<div class="row">


            <div class="col-xs-12 col-lg-12  col-sm-12 col-md-12 pull-left contact2-wrap">

                    <!--Contact form-->
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 no-pad wow fadeInLeft" data-wow-delay="0.5s" data-wow-offset="100">

                    	<form class="contact2-page-form col-lg-8 col-sm-12 col-md-8 col-xs-12 no-pad contact-v2" id="contactForm">


                        <div class="form-title-text no-pad">Contáctanos</div>


                            <div class="alert alert-success hidden col-lg-12 col-sm-12 col-md-12 col-xs-12" id="contactSuccess">
								<strong>Success!</strong> Tu mensaje ha sido enviado.
							</div>

							<div class="alert alert-error hidden col-lg-12 col-sm-12 col-md-12 col-xs-12" id="contactError">
								<strong>Error!</strong> Ocurrió un error al enviar tu mensaje.
							</div>

                        	<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                        	<input type="text" class="contact2-textbox" placeholder="Nombre*" name="name" id="name" />
                            </div>

                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                            <input type="email" class="contact2-textbox" placeholder="Email*" name="email" id="email"/>
                            </div>

                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                            <input type="text" placeholder="Asunto" class="contact2-textbox" name="subject" id="subject">
                            </div>



                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                           <textarea class="contact2-textarea" placeholder="Escribe tu mensaje" name="message" id="message"></textarea>
                            </div>

                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            <section class="color-7" id="btn-click">
                <button class="icon-mail btn2-st2 btn-7 btn-7b" data-loading-text="Loading..." type="submit">Enviar</button>

                </section>
                            </div>


                        </form>


                        <!--Contact Sidebar-->
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">


                    	<div class="side-bar-contact">
                        	<div class="form-title-text wow fadeInRight" data-wow-delay="0.5s" data-wow-offset="100">Contacto</div>
                            <ul class="contact-page-list wow fadeInRight" data-wow-delay="0.5s" data-wow-offset="100">
                            <li>
                                <i class="icon-globe contact-side-icon iside-icon-contact"></i>
                            <span class="contact-side-txt">Sociedad Veracruzana de Radiología e Imagen A.C. Hidalgo 401 Centro C.P. 96400 Coatzacoalcos Veracruz.</span>
                            </li><br>
                            <li>
                                <i class="icon-phone2 contact-side-icon"></i>
                            <span class="contact-side-txt">Teléfono: <span class="iside-bar-cfont">+52 921 212 8132
                            </span></span>
                            </li>
                            <li>
                                <i class="icon-mail contact-side-icon"></i>
                                <span class="contact-side-txt">Email: <a href="mailto:contactanos@svri.org.mx">contactanos@svri.org.mx</a></span>
                            </li>
                            </ul>

                            <div class="form-title-text wow fadeInRight" data-wow-delay="0.5s" data-wow-offset="120">Redes sociales</div>

                            <ul class="contact-page-social-list-bottom contact-page-social-list wow fadeInRight" data-wow-delay="0.5s" data-wow-offset="120">

                            <li><a href="https://www.facebook.com/SVRIMG">
                            <div class="contact-side-social-wrap">
                            <i class="icon-facebook contact-side-social-icon"></i></div></a>
                            </li>

                            </ul>
                        </div>

                    </div><!--Contact Sidebar end-->


                    </div><!--Contact Form end-->





                </div>

            </div>

        </div>

         </div>


    <!--Mid Content End-->


        <!--Footer Start-->

    </section>

    <section class="complete-footer">

    <div class="bottom-footer">
    <div class="container">

        <div class="row">
            <!--Foot widget-->
            <div class="col-xs-12 col-sm-12 col-md-12 foot-widget-bottom">
            <p class="col-xs-12 col-md-5 no-pad">MAGEST Software | All Rights Reserved </p>
            <ul class="foot-menu col-xs-12 col-md-7 no-pad">

            <li><a href="contacto.php">Contacto</a></li>
            <li><a href="links_rad.php">Links radiológicos</a></li>
            <li><a  href="verimagenes.php">VerImagenes</a></li>
            <li><a href="publico_gral.php">Público en general</a></li>
            <li><a href="quienes_somos.php">¿Quiénes somos?</a></li>
            <li><a href="index.php">Inicio</a></li>



            </ul>
            </div>
        </div>
    </div>
    </div>

    </section>


    <!--JS Inclution-->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="bootstrap-new/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="js/jquery.scrollUp.min.js"></script>
    <script type="text/javascript" src="js/jquery.sticky.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/jquery.flexisel.min.js"></script>
    <script type="text/javascript" src="js/jquery.imedica.min.js"></script>
    <script type="text/javascript" src="js/custom-imedicajs.min.js"></script>

    <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
    <script type="text/javascript">
            $("#map-canvas-2").gMap({

            styles:[{stylers:[
        {
            featureType: 'water', // set the water color
            elementType: 'geometry.fill', // apply the color only to the fill
            stylers: [
                { color: '#adc9b8' }
            ]
        },{
            featureType: 'landscape.natural', // set the natural landscape
            elementType: 'all',
            stylers: [
                { hue: '#809f80' },
                { lightness: -35 }
            ]
        }
        ,{
            featureType: 'poi', // set the point of interest
            elementType: 'geometry',
            stylers: [
                { hue: '#f9e0b7' },
                { lightness: 30 }
            ]
        },{
            featureType: 'road', // set the road
            elementType: 'geometry',
            stylers: [
                { hue: '#d5c18c' },
                { lightness: 14 }
            ]
        },{
            featureType: 'road.local', // set the local road
            elementType: 'all',
            stylers: [
                { hue: '#ffd7a6' },
                { saturation: 100 },
                { lightness: -12 }
            ]
        }
    ]}],
            controls: false,
            scrollwheel: false,
            maptype: 'ROADMAP',
            markers: [
                {
                    latitude: 18.1452816,
                    longitude: -94.4157737,
                    icon: {
                        image: "images/location2.png",
                        iconsize: [50, 50],
                        iconanchor: [50,50]
                    }
                },

            ],
            icon: {
                image: "images/location2.png",
                iconsize: [50, 50],
                iconanchor: [50, 50]
            },
            latitude: 18.1452816,
            longitude: -94.4157737,

            zoom: 12,
            mapTypeId: 'Styled'


        });

        </script>

</body>
</html>
