<!DOCTYPE HTML>
<!--[if gt IE 8]> <html class="ie9" lang="en"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


  <title>SVRI</title>

  <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
  <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
  <link href="css/animate.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/green.css" id="style-switch" />

  <!-- REVOLUTION BANNER CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

  <!--[if IE 9]>
      <link rel="stylesheet" type="text/css" href="css/ie9.css" />
    <![endif]-->

  <link rel="icon" type="image/png" href="images/LOGO.svg">
  <link rel="stylesheet" type="text/css" href="css/inline.min.css" />
</head>

<body>

  <?php include 'menuPrincipal.html'; ?>

  <section class="complete-content content-footer-space">
    <div id="gallery-columns-carousel">
      <!--Mid Content Start-->


      <div class="about-intro-wrap pull-left">

        <div class="bread-crumb-wrap ibc-wrap-1">
          <div class="container">
            <!--Title / Beadcrumb-->
            <div class="inner-page-title-wrap col-xs-12 col-md-12 col-sm-12">
              <div class="bread-heading">
                <h1>Galería</h1></div>
              <div class="bread-crumb pull-right">
                <ul>
                  <li><a href="index.php">Inicio</a></li>
                  <li><a href="quienes_somos.php">¿Quiénes somos?</a></li>
                  <li><a href="galeria.php">Galería</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div class="container">
          <!--  -->
          <div class="tab-pane fade fade-slow in active" id="all-doc">

            <!--Doc intro-->
            <div class="doctor-box col-md-6 col-sm-12 col-xs-12 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-offset="200">
              <div class="zoom-wrap">
                <div class="zoom-icon"></div>
                <img alt="" class="img-responsive SVRIgalery" src="images/galeria/Socios2004.jpg"/>
              </div>
              <div class="doc-name">
                <div class="doc-name-class">Socios 2004</div>
              </div>
            </div>

            <div class="doctor-box col-md-6 col-sm-12 col-xs-12 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-offset="200">
              <div class="zoom-wrap">
                <div class="zoom-icon"></div>
                <img alt="" class="img-responsive" src="images/galeria/socios2005.jpg" />
              </div>
              <div class="doc-name">
                <div class="doc-name-class">Socios 2005</div>
              </div>
            </div>

            <div class="doctor-box col-md-6 col-sm-12 col-xs-12 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-offset="200">
              <div class="zoom-wrap">
                <div class="zoom-icon"></div>
                <img alt="" class="img-responsive" src="images/galeria/socios2006.jpg" />
              </div>
              <div class="doc-name">
                <div class="doc-name-class">Socios 2006</div>
              </div>
            </div>

            <div class="doctor-box col-md-6 col-sm-12 col-xs-12 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-offset="200">
              <div class="zoom-wrap">
                <div class="zoom-icon"></div>
                <img alt="" class="img-responsive" src="images/galeria/socios_2007.jpg" />
              </div>
              <div class="doc-name">
                <div class="doc-name-class">Socios 2007</div>
              </div>
            </div>

            <div class="doctor-box col-md-6 col-sm-12 col-xs-12 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-offset="200">
              <div class="zoom-wrap">
                <div class="zoom-icon"></div>
                <img alt="" class="img-responsive" src="images/galeria/socios_2008.jpg" />
              </div>
              <div class="doc-name">
                <div class="doc-name-class">Socios 2008</div>
              </div>
            </div>

            <div class="doctor-box col-md-6 col-sm-12 col-xs-12 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-offset="200">
              <div class="zoom-wrap">
                <div class="zoom-icon"></div>
                <img alt="" class="img-responsive" src="images/galeria/socios_2009.jpg" />
              </div>
              <div class="doc-name">
                <div class="doc-name-class">Socios 2009</div>
              </div>
              </div>

            <div class="doctor-box col-md-6 col-sm-12 col-xs-12 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-offset="200">
              <div class="zoom-wrap">
                <div class="zoom-icon"></div>
                <img alt="" class="img-responsive" src="images/galeria/socios_2010.jpg" />
              </div>
              <div class="doc-name">
                <div class="doc-name-class">Socios 2010</div>
              </div>
            </div>
            
          <div class="doctor-box col-md-6 col-sm-12 col-xs-12 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-offset="200">
              <div class="zoom-wrap">
                <div class="zoom-icon"></div>
                <img alt="" class="img-responsive" src="images/galeria/socios2011.jpg" />
              </div>
              <div class="doc-name">
                <div class="doc-name-class">Socios 2011</div>
              </div>
            </div>
            
            <div class="doctor-box col-md-6 col-sm-12 col-xs-12 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-offset="200">
              <div class="zoom-wrap">
                <div class="zoom-icon"></div>
                <img alt="" class="img-responsive" src="images/galeria/SOCIOS2012.jpg" />
              </div>
              <div class="doc-name">
                <div class="doc-name-class">Socios 2012</div>
              </div>
            </div>
            
            <div class="doctor-box col-md-6 col-sm-12 col-xs-12 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-offset="200">
              <div class="zoom-wrap">
                <div class="zoom-icon"></div>
                <img alt="" class="img-responsive" src="images/galeria/socios2013.jpg" />
              </div>
              <div class="doc-name">
                <div class="doc-name-class">Socios 2013</div>
              </div>
            </div>
            
            <div class="doctor-box col-md-6 col-sm-12 col-xs-12 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-offset="200">
              <div class="zoom-wrap">
                <div class="zoom-icon"></div>
                <img alt="" class="img-responsive" src="images/galeria/socios2014.jpg" />
              </div>
              <div class="doc-name">
                <div class="doc-name-class">Socios 2014</div>
           </div>
          </div>
         
            <div class="doctor-box col-md-6 col-sm-12 col-xs-12 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-offset="200">
              <div class="zoom-wrap">
                <div class="zoom-icon"></div>
                <img alt="" class="img-responsive" src="images/galeria/socios2015.jpg" />
              </div>
              <div class="doc-name">
                <div class="doc-name-class">Socios 2015</div>
           </div>
          </div>
          
          <div class="doctor-box col-md-6 col-sm-12 col-xs-12 wow fadeInUp animated animated" data-wow-delay="0.5s" data-wow-offset="200">
              <div class="zoom-wrap">
                <div class="zoom-icon"></div>
                <img alt="" class="img-responsive" src="images/galeria/socios2016.jpg" />
              </div>
              <div class="doc-name">
                <div class="doc-name-class">Socios 2016</div>
           </div>
          </div>

        </div>
    <!--Footer Start-->
    </div>
    <!--gallery-columns-carousel-->
  </section>

  <section class="complete-footer">
    <div class="bottom-footer">
      <div class="container">

        <div class="row">
          <!--Foot widget-->
          <div class="col-xs-12 col-sm-12 col-md-12 foot-widget-bottom">
            <p class="col-xs-12 col-md-5 no-pad">MAGEST Software 2015 | All Rights Reserved</p>
            <ul class="foot-menu col-xs-12 col-md-7 no-pad">

              <li><a href="contacto.php">Contacto</a></li>
              <li><a href="links_rad.php">Links radiológicos</a></li>
              <li><a  href="verimagenes.php">VerImagenes</a></li>
              <li><a href="publico_gral.php">Público en general</a></li>
              <li><a href="quienes_somos.php">¿Quiénes somos?</a></li>
              <li><a href="index.php">Inicio</a></li>



            </ul>
          </div>
        </div>
      </div>
    </div>

  </section>

  <!--JS Inclution-->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
  <script type="text/javascript" src="bootstrap-new/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
  <script type="text/javascript" src="js/jquery.scrollUp.min.js"></script>
  <script type="text/javascript" src="js/jquery.sticky.min.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/jquery.flexisel.min.js"></script>
  <script type="text/javascript" src="js/jquery.imedica.min.js"></script>
  <script type="text/javascript" src="js/custom-imedicajs.min.js"></script>

</body>

</html>
