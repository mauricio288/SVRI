<!DOCTYPE HTML>
<!--[if gt IE 8]> <html class="ie9" lang="en"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


  <title>SVRI</title>

  <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

  <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
  <link href="css/animate.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/green.css" id="style-switch" />

  <!-- REVOLUTION BANNER CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

  <!--[if IE 9]>
      <link rel="stylesheet" type="text/css" href="css/ie9.css" />
    <![endif]-->

  <link rel="icon" type="image/png" href="images/LOGO.svg">
  <link rel="stylesheet" type="text/css" href="css/inline.min.css" />
</head>

<body>

<?php include 'menuPrincipal.html' ?>
  <!--Mid Content Start-->


  <section class="complete-content content-footer-space">

    <div class="about-intro-wrap pull-left">

      <div class="bread-crumb-wrap ibc-wrap-2">
        <div class="container">
          <!--Title / Beadcrumb-->
          <div class="inner-page-title-wrap col-xs-12 col-md-12 col-sm-12">
            <div class="bread-heading">
              <h1>Socios</h1></div>
            <div class="bread-crumb pull-right">
              <ul>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="quienes_somos.php">Quienes Somos</a></li>
                <li><a href="socios.php">Socios</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="container">

        <!--Bottom Services-->
        <div class="services-bottom-wrap pull-left">
          <div class="container">

            <div class="row">

              <div class="col-md-12 col-xs-12 col-sm-12 pull-left full-content wow fadeInLeft animated" data-wow-delay="0.5s" data-wow-offset="130">
                <div class="full-content-title">Socios Activos</div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Alberto_Cuevas_Trujillo.jpg" /></a>
                  <div class="bottom-service-title">Dr. Alberto Cuevas Trujillo.</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Alberto_Montemayor_Martinez.jpg" /></a>
                  <div class="bottom-service-title">Dr. Alberto Montemayor Martínez</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Alejandro_Sanchez_Panes.jpg" /></a>
                  <div class="bottom-service-title">Dr. Alejandro Sánchez Panes.
                  </div>

                </div>

                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Alejandro_Schleske_Ruiz.jpg" /></a>
                  <div class="bottom-service-title">Dr. Alejandro Schleske Ruiz.</div>

                </div>


              </div>
              <!--- segunda fila-->

              <div class="row">

                <!--bottom-service-box-->


                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Alicia_Tepo_Carmona.jpg" /></a>
                  <div class="bottom-service-title">Dra. Alicia Tepo Carmona.
                  </div>

                </div>
                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Amada_Victoriano_Cruz.jpg" /></a>
                  <div class="bottom-service-title">Dra. Amada Victoriano Cruz.</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Ana_C_Velazquez_Leyva.jpg" /></a>
                  <div class="bottom-service-title">Dra. Ana C. Velázquez Leyva. </div>

                </div>

                <!--bottom-service-box-->
<div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Antonio_Guarniola_Fernandez.jpg" /></a>
                  <div class="bottom-service-title">Dr. Antonio Guarniola Fernández</div>
              </div>
</div>
<!--- tercera fila-->

              <div class="row">

                <!--bottom-service-box-->


                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Arturo_Albrandt_Salmeron.jpg" /></a>
                  <div class="bottom-service-title">Dr. Alberto Albrandt Salmeron
                  </div>

                </div>
                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Arturo_Cruz_Pando.jpg" /></a>
                  <div class="bottom-service-title">Dr. Arturo Cruz Pando.</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Aucencio_Adrian_Lopez_Contreras.jpg" /></a>
                  <div class="bottom-service-title">Dr. Aucencio Adrian López Contreras. </div>

                </div>

                <!--bottom-service-box-->
<div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Beatriz_Alvarado_Monterrubio.jpg" /></a>
                  <div class="bottom-service-title">Dra. Beatriz Alvarado Monterrubio</div>
              </div>

</div>           
<!--- cuarta fila-->

              <div class="row">

                <!--bottom-service-box-->


                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Carlos_Gonzalez_Pimentel.jpg" /></a>
                  <div class="bottom-service-title">Dr. Carlos González Pimentel.
                  </div>

                </div>
                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Carlos_R_Mora_Basurto.jpg" /></a>
                  <div class="bottom-service-title">Dr.Carlos R. Mora Basurto.</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Carlos_R_Penagos_Gonzalez.jpg" /></a>
                  <div class="bottom-service-title">Dr. Carlos R. Penagos González. </div>

                </div>

                <!--bottom-service-box-->
<div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Cruz_A_Guinare_Siso.jpg" /></a>
                  <div class="bottom-service-title">Dr. Cruz A. Guinare Siso.</div>
             </div>
</div>
<!--- quinta fila-->

              <div class="row">

                <!--bottom-service-box-->


                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/David_Seseña_Lopez.jpg" /></a>
                  <div class="bottom-service-title">Dr. David Seseña López. </div>
                  </div>

                
                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Eduardo_Garcia_Rebollal.jpg" /></a>
                  <div class="bottom-service-title">Dr. Eduardo Garcia Rebollal.</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Emma_Camacho_Campos.jpg" /></a>
                  <div class="bottom-service-title">Dra. Emma Camacho Campos. </div>

                </div>
                   <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Enrique_Mainero_Crespo.jpg" /></a>
                  <div class="bottom-service-title">Dr. Enrique Mainero Crespo.
                  </div>
</div>
<!--- sexta fila-->

              <div class="row">

                <!--bottom-service-box-->


                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Federico_Grosskelwing_Acosta.jpg" /></a>
                  <div class="bottom-service-title">Dr. Federico Grosskelwing Acosta.
                  </div>

                </div>
                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Fernando_Ramirez_Lizarraga .jpg" /></a>
                  <div class="bottom-service-title">Dr. Fernando Ramírez Lizarraga.</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Horacio_Deschams_Diaz.jpg" /></a>
                  <div class="bottom-service-title">Dr. Horacio Deschams Diaz. </div>

                </div>

                <!--bottom-service-box-->
<div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Hortencia_Seferino_Heredia .jpg" /></a>
              <div class="bottom-service-title">Dra. Hortencia Zeferino Heredia.</div>
             </div>
            </div>

<!--- septima fila-->

              <div class="row">

                <!--bottom-service-box-->


                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Humberto_Gonzalez_Prado.jpg" /></a>
                  <div class="bottom-service-title">Dr. Humberto González Prado.
                  </div>

                </div>
                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Jaime_Simonin_Cruz.jpg" /></a>
                  <div class="bottom-service-title">Dr. Jaime Simonin Cruz.</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Jenaro_Herrera_Olivarez.jpg" /></a>
                  <div class="bottom-service-title">Dr. Jenaro Herrera Olivares. </div>

                </div>

                <!--bottom-service-box-->
<div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Jorge_A_Pacheco_Pacheco.jpg" /></a>
              <div class="bottom-service-title">Dr. Jorge Adalberto Pacheco Pacheco.</div>
             </div>
            </div>
            
            <!--- octava fila-->

              <div class="row">

                <!--bottom-service-box-->


                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Jorge_Herrera_Cantillo.jpg" /></a>
                  <div class="bottom-service-title">Dr. Jorge Herrera Cantillo.
                  </div>

                </div>
                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Jorge_Martinez_Vela.jpg" /></a>
                  <div class="bottom-service-title">Dr. Jorge Martínez Vela.</div>
                </div>
                
               <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Jose_Hugo_Arredondo_Estrada.jpg" /></a>
                  <div class="bottom-service-title">Dr. José Hugo Arredondo Estrada.</div>
                  </div>
                  <!--bottom-service-box-->
                 <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Jose_Luis_Alvarado.jpg" /></a>
                  <div class="bottom-service-title">Dr. Jorge Luis Velazquez Corona.</div>
                </div>
            </div>
            
             <!--- novena fila-->

              <div class="row">

                <!--bottom-service-box-->


                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Jose_M_Martinez_Segura.jpg" /></a>
                  <div class="bottom-service-title">Dr. José María Martínez Segura.
                  </div>

                </div>
                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Juan_A_Ramirez_Casanova.jpg" /></a>
                  <div class="bottom-service-title">Dr. Juan Adrian Ramírez Casanova.</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Julian_Salome_Salome.jpg" /></a>
                  <div class="bottom-service-title">Dr. Julian Salomé Salomé. </div>

                </div>

                <!--bottom-service-box-->
<div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Jose_Luis_Rivera.jpg" /></a>
              <div class="bottom-service-title">Dr. José Luis Rivera Calderón.</div>
             </div>
            </div>
            
            <!--- decima fila-->

              <div class="row">

                <!--bottom-service-box-->


                
                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Juan_A_Ramirez_Casanova.jpg" /></a>
                  <div class="bottom-service-title">Dr. Juan Adrian. Ramírez Casanova.</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Julian_Salome_Salome.jpg" /></a>
                  <div class="bottom-service-title">Dr. Julian Salomé Salomé. </div>

                </div>

                <!--bottom-service-box-->
<div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Julio_Cesar_Diaz_Avendano.jpg" /></a>
              <div class="bottom-service-title">Dr. José Artemio Benavides Díaz.</div>
             </div>
           
            
            <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Luis_Eduardo_Mendez_Casarin.jpg" /></a>
                  <div class="bottom-service-title">Dr. Luis Eduardo Mendez Casarin.</div>
                  </div>

                </div>
            
            <!--- decimo primera fila-->

              <div class="row">

                <!--bottom-service-box-->


                
                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Maria_Del_Sagrario_Lastra_Guerra.jpg" /></a>
                  <div class="bottom-service-title">Dra. María Del Sagrario Lastra Guerra.</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Maria_Julia_Valdez.jpg" /></a>
                  <div class="bottom-service-title">Dra. María Julia Valdez. </div>

                </div>

                <!--bottom-service-box-->
<div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Maria_Luisa_De_La_Cruz_Arias.jpg" /></a>
              <div class="bottom-service-title">Dra. María Luisa De La Cruz Arias.</div>
             </div>
           
            
            <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Maria_Magdalena_Mendoza_Alegria.jpg" /></a>
                  <div class="bottom-service-title">Dra. María Magdalena Mendoza Alegría.</div>
                  </div>

                </div>
                
                <!--- decimo segunda fila-->

              <div class="row">

                <!--bottom-service-box-->


                
                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Maria_T_Orta_Gil.jpg" /></a>
                  <div class="bottom-service-title">Dra. María Teresa Orta Gil.</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Martha_Patricia_Garcia_Barenque.jpg" /></a>
                  <div class="bottom-service-title">Dra. Martha Patricia García Barenque. </div>

                </div>

                <!--bottom-service-box-->
<div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Minerva_Peez_Perez.jpg" /></a>
              <div class="bottom-service-title">Dra. Minerva Pérez Pérez.</div>
             </div>
           
            
            <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Muñoz_Gonzalez.jpg" /></a>
                  <div class="bottom-service-title">Dra. Josefina García González.</div>
                  </div>

                </div>
                
                <!--- decimo tercera fila-->

              <div class="row">

                <!--bottom-service-box-->


                
                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Noe_Ramirez_Hernandez.jpg" /></a>
                  <div class="bottom-service-title">Dr. Noé Ramírez Hernández.</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Publio_Saldana_Ruiz.jpg" /></a>
                  <div class="bottom-service-title">Dr. Publio Saldaña Ruíz. </div>

                </div>

                <!--bottom-service-box-->
<div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Rafael_Hernandez_Gonzalez.jpg" /></a>
              <div class="bottom-service-title">Dr. Rafael Hernández González.</div>
             </div>
           
            
            <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Ramon_Hernandez_Terrazas.jpg" /></a>
                  <div class="bottom-service-title">Dr. Ramon Hernánez Terrazas.</div>
                  </div>

                </div>
                
                <!--- decimo cuarta fila-->

              <div class="row">

                <!--bottom-service-box-->


                
                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Rosa_Morales_Blanca.jpg" /></a>
                  <div class="bottom-service-title">Dra. Blanca Rosa Morales.</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Roxana_Siu_Munoz.jpg" /></a>
                  <div class="bottom-service-title">Dra. Roxana Siu Muñoz. </div>

                </div>

                <!--bottom-service-box-->
<div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Ruben_Tenorio_Covarrubias.jpg" /></a>
              <div class="bottom-service-title">Dr. Ruben Tenorio Covarrubias.</div>
             </div>
           
            
            <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Sara_Benitez_Palafox.jpg" /></a>
                  <div class="bottom-service-title">Dra. Sára Benítez Palafox.</div>
                  </div>

                </div>
                
                <!--- decimo quinta fila-->

              <div class="row">

                <!--bottom-service-box-->


                
                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Teodoro_Galicia_Diaz.jpg" /></a>
                  <div class="bottom-service-title">Dr. Teodoro Galicia Díaz</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Urania_Sanchez_Hernandez.jpg" /></a>
                  <div class="bottom-service-title">Dra. Urania Sánchez Hernández. </div>

                </div>

                <!--bottom-service-box-->
<div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Victor_Manuel_Cazares_Magana.jpg" /></a>
              <div class="bottom-service-title">Dr. Víctor Manuel Cáceres Magaña.</div>
             </div>
           
            
            <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Zoraida_Baxin_Uscanga.jpg" /></a>
                  <div class="bottom-service-title">Dra. Zoraida Baxin Uscanga.</div>
                  </div>

                </div>
            <!-- socios correspondientes-->
            <div class="row" style="display:none">
            

              <div class="col-md-12 col-xs-12 col-sm-12 pull-left full-content wow fadeInLeft animated" data-wow-delay="0.5s" data-wow-offset="130">
                <div class="full-content-title">Socios Correspondientes</div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Antonio_Guarniola_Fernandez.jpg" /></a>
                  <div class="bottom-service-title">Dr. Antonio Guarniola Fernández</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="#"><img alt="" class="img-responsive" src="socios/Antonio_Rodriguez_Guzman.jpg" /></a>
                  <div class="bottom-service-title">Dr. Antonio Rodríguez Guzmán</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="seram.es"><img alt="" class="img-responsive" src="images/sociedades/seram.jpg" /></a>
                  <div class="bottom-service-title">Sociedad Española de Radiología Médica</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="http://www.myesr.org/cms/website.php?id=/en/ESR_ECR_news.htm"><img alt="" class="img-responsive" src="images/sociedades/esr.jpg" /></a>
                  <div class="bottom-service-title">European Society of Radiology
                  </div>

                </div>

              </div>
              <!--- segunda fila-->

              <div class="row">

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.sar.org.ar/web/index.php"><img alt="" class="img-responsive" src="images/sociedades/sar.jpg" /></a>
                  <div class="bottom-service-title">Sociedad Argentina de radiología</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="http://www.slarp.net/"><img alt="" class="img-responsive" src="images/sociedades/slarp.jpg" /></a>
                  <div class="bottom-service-title">Sociedad Latinoamericana de Radiología Pediátrica</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://cbr.org.br/"><img alt="" class="img-responsive" src="images/sociedades/cbr.jpg" /></a>
                  <div class="bottom-service-title">Colegio Brasileiro de Radiología</div>

                </div>

                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.sochradi.cl/web/"><img alt="" class="img-responsive" src="images/sociedades/scr.jpg" /></a>
                  <div class="bottom-service-title">Sociedad Chilena de Radiología</div>

                </div>


                <!--bottom-service-box-->

              </div>


              <div class="row">

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.acronline.org/"><img alt="" class="img-responsive" src="images/sociedades/acr.gif" /></a>
                  <div class="bottom-service-title">Asociación Colombiana de Radiología</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="http://www.car.ca/"><img alt="" class="img-responsive" src="images/sociedades/car.jpg" /></a>
                  <div class="bottom-service-title">Canadian Association of Radiologists </div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.sru.org/"><img alt="" class="img-responsive" src="images/sociedades/sru.jpg" /></a>
                  <div class="bottom-service-title">Society of radiologists in ultrasound </div>

                </div>

                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.ismrm.org/"><img alt="" class="img-responsive" src="images/sociedades/ismrm.jpg" /></a>
                  <div class="bottom-service-title">International Society for Magnetic Resonance in Medicine</div>

                </div>


                <!--bottom-service-box-->

              </div>

              <div class="row">

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.acr.org/"><img alt="" class="img-responsive" src="images/sociedades/acr.png" /></a>
                  <div class="bottom-service-title">American College of Radiology</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="http://www.isradiology.org/isr/index.php"><img alt="" class="img-responsive" src="images/sociedades/isr.jpg" /></a>
                  <div class="bottom-service-title">International Society of Radiology</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.essr.org/cms/website.php?id=/en/essr_home.htm"><img alt="" class="img-responsive" src="images/sociedades/essr.gif" /></a>
                  <div class="bottom-service-title">European Society of Musculoskeletal Radiology</div>

                </div>

                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="https://skeletalrad.org/"><img alt="" class="img-responsive" src="images/sociedades/ssr.png" /></a>
                  <div class="bottom-service-title">Society of Skeletal Radiology</div>

                </div>


                <!--bottom-service-box-->

              </div>

              <div class="row">

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.musoc2015.in/"><img alt="" class="img-responsive" src="images/sociedades/musoc.jpg" /></a>
                  <div class="bottom-service-title">Musculoskeletal Society</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="http://www.sirweb.org/"><img alt="" class="img-responsive" src="images/sociedades/sir.jpg" /></a>
                  <div class="bottom-service-title">Society of Interventional Radiology
                  </div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.pedrad.org/"><img alt="" class="img-responsive" src="images/sociedades/pedrad.jpg" /></a>
                  <div class="bottom-service-title">The Society of Pediatric Radiology</div>

                </div>

                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.soveradi.com.ve/"><img alt="" class="img-responsive" src="images/sociedades/soveradi.gif" /></a>
                  <div class="bottom-service-title">Sociedad Venezolana de Radiología </div>

                </div>


                <!--bottom-service-box-->

              </div>




              <div class="row">

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.brighamandwomens.org/"><img alt="" class="img-responsive" src="images/sociedades/bwh.jpg" /></a>
                  <div class="bottom-service-title">Brigham and Women's Hospital</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="http://www.sociedadbolivianaderadiologia.com/sbr/"><img alt="" class="img-responsive" src="images/sociedades/sbr.jpg" /></a>
                  <div class="bottom-service-title">Sociedad Boliviana de Radiología
                  </div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.sedim.es/"><img alt="" class="img-responsive" src="images/sociedades/sedim.jpg" /></a>
                  <div class="bottom-service-title">Sociedad Española de Diagnostico por Imagen de la Mama</div>

                </div>

                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.sriuy.org.uy/"><img alt="" class="img-responsive" src="images/sociedades/sriuy.png" /></a>
                  <div class="bottom-service-title">Soc. de Radiología e Imagenología del Uruguay.
                  </div>

                </div>


                <!--bottom-service-box-->

              </div>



              <div class="row">

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.sopari.org/"><img alt="" class="img-responsive" src="images/sociedades/sopari.jpg" /></a>
                  <div class="bottom-service-title">Sociedad Panameña de Radiología e Imagen</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="http://www.pediatricradiology.com/"><img alt="" class="img-responsive" src="images/sociedades/pr.jpg" /></a>
                  <div class="bottom-service-title">Pediatic Radiology
                  </div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.seri.org.ec/"><img alt="" class="img-responsive" src="images/sociedades/seri.png" /></a>
                  <div class="bottom-service-title">Sociedad Ecuatoriana de Radiología</div>

                </div>

                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.asoradgua.com.gt/"><img alt="" class="img-responsive" src="images/sociedades/asoradgu.jpg" /></a>
                  <div class="bottom-service-title">Asociación Civil de Diagnóstico por Imágenes y Terapia Radiante de Guatemala
                  </div>

                </div>


                <!--bottom-service-box-->

              </div>






            </div>


          </div>



        </div>
      </div>

    </div>








  </section>



  <!--Mid Content End-->


  <section class="complete-footer">
    <div class="bottom-footer">
      <div class="container">

        <div class="row">
          <!--Foot widget-->
          <div class="col-xs-12 col-sm-12 col-md-12 foot-widget-bottom">
            <p class="col-xs-12 col-md-5 no-pad">MAGEST Software 2015 | All Rights Reserved </p>
            <ul class="foot-menu col-xs-12 col-md-7 no-pad">

              <li><a href="contacto.php">Contacto</a></li>
            <li><a href="links_rad.php">Links radiológicos</a></li>
            <li><a  href="verimagenes.php">VerImagenes</a></li>
            <li><a href="publico_gral.php">Público en general</a></li>
            <li><a href="quienes_somos.php">¿Quiénes somos?</a></li>
            <li><a href="index.php">Inicio</a></li>


            </ul>
          </div>
        </div>
      </div>
    </div>

  </section>


  <!--JS Inclution-->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
  <script type="text/javascript" src="bootstrap-new/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
  <script type="text/javascript" src="js/jquery.scrollUp.min.js"></script>
  <script type="text/javascript" src="js/jquery.sticky.min.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/jquery.flexisel.min.js"></script>
  <script type="text/javascript" src="js/jquery.imedica.min.js"></script>
  <script type="text/javascript" src="js/custom-imedicajs.min.js"></script>

</body>

</html>
