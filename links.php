<!DOCTYPE HTML>
<!--[if gt IE 8]> <html class="ie9" lang="en"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


  <title>SVRI</title>

  <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

  <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
  <link href="css/animate.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/green.css" id="style-switch" />

  <!-- REVOLUTION BANNER CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

  <!--[if IE 9]>
      <link rel="stylesheet" type="text/css" href="css/ie9.css" />
    <![endif]-->

  <link rel="icon" type="image/png" href="images/LOGO.svg">
  <link rel="stylesheet" type="text/css" href="css/inline.min.css" />
</head>

<body>

  <?php include 'menuPrincipal.html'; ?>

  <!--Mid Content Start-->


  <section class="complete-content content-footer-space">

    <div class="about-intro-wrap pull-left">

      <div class="bread-crumb-wrap ibc-wrap-1">
        <div class="container">
          <!--Title / Beadcrumb-->
          <div class="inner-page-title-wrap col-xs-12 col-md-12 col-sm-12">
            <div class="bread-heading">
              <h1>Links de interés</h1></div>
            <div class="bread-crumb pull-right">
              <ul>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="links_rad.php">Links radiológicos</a></li>
                <li><a href="links.php">Links de interés</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="container">




        <!--Bottom Services-->
        <div class="services-bottom-wrap pull-left">
          <div class="container">

            <div class="row">

              <div class="col-md-12 col-xs-12 col-sm-12 pull-left full-content wow fadeInLeft animated" data-wow-delay="0.3s" data-wow-offset="130">
                <div class="full-content-title">Nacionales</div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.inr.gob.mx/"target="_blank"><img alt="" class="img-responsive" src="images/links/inr.gif" /></a>
                  <div class="bottom-service-title">Instituto Nacional de Rehabilitación</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="https://www.cardiologia.org.mx/"target="_blank"><img alt="" class="img-responsive" src="images/links/inc.jpg" /></a>
                  <div class="bottom-service-title">Instituto Nacional de Cardiología.</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.innn.salud.gob.mx/"target="_blank"><img alt="" class="img-responsive" src="images/links/innn.png" /></a>
                  <div class="bottom-service-title">Instituto Nacional de Neurología y Neurocirugía</div>

                </div>

                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.pediatria.gob.mx/"target="_blank"><img alt="" class="img-responsive" src="images/links/inp.jpg" /></a>
                  <div class="bottom-service-title">Instituto Nacional de Pediatría</div>

                </div>


              </div>
              <!--- segunda fila-->

              <div class="row">

                <!--bottom-service-box-->


                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="http://www.incan.salud.gob.mx/"target="_blank"><img alt="" class="img-responsive" src="images/links/inc.png" /></a>
                  <div class="bottom-service-title">Instituto Nacional de Cancerología</div>

                </div>
                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="http://www.innsz.mx/opencms/index.html"target="_blank"><img alt="" class="img-responsive" src="images/links/incmn.jpg" /></a>
                  <div class="bottom-service-title">Instituto Nacional de Ciencias Médicas y de la Nutrición</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.iner.salud.gob.mx/"target="_blank"><img alt="" class="img-responsive" src="images/links/iner.jpg" /></a>
                  <div class="bottom-service-title">Instituto Nacional de Enfermedades Respiratorias </div>

                </div>

                <!--bottom-service-box-->

                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.inper.edu.mx/"target="_blank"><img alt="" class="img-responsive" src="images/links/inper.jpg" /></a>
                  <div class="bottom-service-title">Instituto Nacional de Perinatología </div>

                </div>


                <!--bottom-service-box-->
              </div>


              <div class="row">


                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.insp.mx/"target="_blank"><img alt="" class="img-responsive" src="images/links/insp.png" /></a>
                  <div class="bottom-service-title">Instituto Nacional de Salud Pública</div>

                </div>

              </div>


              <!--bottom-service-box-->

              <div class="row">

                <div class="col-md-12 col-xs-12 col-sm-12 pull-left full-content wow fadeInLeft animated" data-wow-delay="0.3s" data-wow-offset="130">
                  <div class="full-content-title">Enlaces Internacionales</div>

                  <!--bottom-service-box-->
                  <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                    <a href="http://www.acr.org/"target="_blank"><img alt="" class="img-responsive" src="images/links/acr.png" /></a>
                    <div class="bottom-service-title">Colegio americano de radiología.</div>

                  </div>

                  <!--bottom-service-box-->
                  <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                    <a href="http://www.auntminnie.com/index.aspx?sec=def"target="_blank"><img alt="" class="img-responsive" src="images/links/am.jpg" /></a>
                    <div class="bottom-service-title">Aunt minnie</div>
                  </div>

                  <!--bottom-service-box-->
                  <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                    <a href="http://www.ctisus.com/"target="_blank"><img alt="" class="img-responsive" src="images/links/ctisus.png" /></a>
                    <div class="bottom-service-title">Ct Situs</div>

                  </div>

                  <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                    <a href="#"><img alt="" class="img-responsive" src="images/links/cr.jpg" /></a>
                    <div class="bottom-service-title">Club de radiologia</div>

                  </div>


                </div>
                <!--- segunda fila-->

                <div class="row">

                  <!--bottom-service-box-->


                  <!--bottom-service-box-->
                  <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                    <a href="#"><img alt="" class="img-responsive" src="images/links/xray.jpg" /></a>
                    <div class="bottom-service-title">X-Ray links</div>

                  </div>
                  <!--bottom-service-box-->
                  <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                    <a href=""><img alt="" class="img-responsive" src="images/links/ecr.jpg" /></a>
                    <div class="bottom-service-title">ECR</div>
                  </div>

                  <!--bottom-service-box-->
                  <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                    <a href="https://www.mypacs.net/mpv4/hss/casemanager"target="_blank"><img alt="" class="img-responsive" src="images/links/mypacnet.jpg" /></a>
                    <div class="bottom-service-title">mypacs.net</div>

                  </div>

                  <!--bottom-service-box-->

                  <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                    <a href="http://www.paho.org/hq/?lang=es"target="_blank"><img alt="" class="img-responsive" src="images/links/ops.jpg" /></a>
                    <div class="bottom-service-title">Organización panamericana de la salud</div>

                  </div>


                  <!--bottom-service-box-->
                </div>


                <div class="row">


                  <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                    <a href="https://www.radiologycentral.org/"target="_blank"><img alt="" class="img-responsive" src="images/links/rc.jpg" /></a>
                    <div class="bottom-service-title">Radiology central</div>

                  </div>

                </div>

              </div>



            </div>


          </div>



        </div>
      </div>

    </div>








  </section>



  <!--Mid Content End-->



  <section class="complete-footer">
    <div class="bottom-footer">
      <div class="container">

        <div class="row">
          <!--Foot widget-->
          <div class="col-xs-12 col-sm-12 col-md-12 foot-widget-bottom">
            <p class="col-xs-12 col-md-5 no-pad">MAGEST Software 2015 | All Rights Reserved </p>
            <ul class="foot-menu col-xs-12 col-md-7 no-pad">

              <li><a href="contacto.php">Contacto</a></li>
            <li><a href="links_rad.php">Links radiológicos</a></li>
            <li><a  href="verimagenes.php">VerImagenes</a></li>
            <li><a href="publico_gral.php">Público en general</a></li>
            <li><a href="quienes_somos.php">¿Quiénes somos?</a></li>
            <li><a href="index.php">Inicio</a></li>


            </ul>
          </div>
        </div>
      </div>
    </div>

  </section>


  <!--JS Inclution-->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
  <script type="text/javascript" src="bootstrap-new/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
  <script type="text/javascript" src="js/jquery.scrollUp.min.js"></script>
  <script type="text/javascript" src="js/jquery.sticky.min.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/jquery.flexisel.min.js"></script>
  <script type="text/javascript" src="js/jquery.imedica.min.js"></script>
  <script type="text/javascript" src="js/custom-imedicajs.min.js"></script>

</body>

</html>
