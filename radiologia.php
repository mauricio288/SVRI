<!DOCTYPE HTML>
<!--[if gt IE 8]> <html class="ie9" lang="en"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


  <title>SVRI</title>

  <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
  <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
  <link href="css/animate.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/green.css" id="style-switch" />

  <!-- REVOLUTION BANNER CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

  <!--[if IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie9.css" />
    <![endif]-->

  <link rel="icon" type="image/png" href="images/LOGO.svg">
  <link rel="stylesheet" type="text/css" href="css/inline.min.css" />
</head>

<body>

 <?php include 'menuPrincipal.html'; ?>

  <section class="complete-content content-footer-space">

    <div class="about-intro-wrap pull-left">

      <div class="bread-crumb-wrap ibc-wrap-1">
        <div class="container">
          <!--Title / Beadcrumb-->
          <div class="inner-page-title-wrap col-xs-12 col-md-12 col-sm-12">
            <div class="bread-heading">
              <h1>Radiología</h1></div>
            <div class="bread-crumb pull-right">
              <ul>
                <li><a href="index.html">Inicio</a></li>
                <li><a href="publico_gral.html">Público en general</a></li>
                <li><a href="radiologia.html">Radiología</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="container">

        <div class="row">

          <!--About-us top-content-->

          <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 column-element">

            <h3>Origen</h3>
            <p>Los Rayos X forman parte del espectro de radiaciones electromagnéticas, al igual que las ondas eléctricas y las de radio, (estos en un extremo), y los rayos infrarrojos, los visibles y los ultravioleta (en la zona media), situándose, junto
              a los rayos cósmicos, al otro extremo del espectro.</p>
            <p>
              Los Rayos X se originan cuando los electrones inciden con muy alta velocidad sobre la materia y son frenados repentinamente. Se produce así la radiación X, de muy distintas longitudes de onda ("espectro continuo"), debido a la diferente velocidad de los
              electrones al chocar. Si la energía del bombardeo de electrones es mayor todavía, se producirá otro tipo de radiaciín, cuyas características dependerán del material del blanco ("radiación característica"). </p>
            <p>La diferente longitud de onda de la radiación determina la calidad o dureza de los rayos X: cuanto menor es la longitud de onda, la radiación se dice más dura, que tiene mayor poder de penetración. A lo contrario se denomina "radiación blanda".</p>
            <center><img src="images/radiologia/rad1.jpg" /> </center>
            <p>El 8 de noviembre de 1895, un profesor de física llamado Wilhelm Conrad Roentgen estaba inclinado encima de su mesa de laboratorio en Wurzburg, Alemania. Él estaba investigando la fluorescencia de los rayos catódicos, pasando electricidad
              a través de tubos llenos de un gas raro, similar a nuestras bombillas fluorescentes. De repente él notó una luz extraña que emanaba de una pantalla pequeña que estaba cerca de la mesa. Â¡No se suponía que esto era parte del experimento!
              </p>
            <p>Fascinado con el nuevo fenómeno, él lo investigó día y noche durante siete semanas. él vio el contorno de los huesos en su mano y entonces lo dirigió a la mano de su esposa. Roentgen comprendió que una "luz invisible" previamente desconocida
              estaba causando la fluorescencia y la imagen resultante (resultó ser una onda electromagnética con una longitud de onda muy corta). Porque "X" en matemáticas indica una cantidad desconocida, él llamó al fenómeno: "Rayos X."</p>
            <p>
              El 28 de diciembre de 1895, el Roentgen escribió sus resultados en el Journal de la Sociedad Médico Física de Wursburg y se volvió inmediatamente famoso. A una reunión en Enero a sus "Rayos X" le llamaron el "el rayo Roentgen". Así de entusiasmado estaba
              el mundo científico sobre este descubrimiento que más de mil artículos y más de cincuenta libros sobre el "rayo Roentgen" se publicaron en el primer año, 1896.
            </p>
            <p>Los "Rayos X" se utilizan en Radiología para efectuar varios estudios como son:

              <ul>
                <li>Estudios (placas) simples.</li>
                <li>Fluoroscopía.</li>
                <li>Estudios contrastados.</li>
                <li>Estudios invasivos.</li>
                <li>Mamografía.</li>
                <li>Tomografía computada.</li>
              </ul>
            </p>
            <p>

              La radiografía simple es la técnica inicial de imagen por excelencia, llegando a ser el primer examen diagnóstico que se realiza después de la historia clínica en la mayoría de pacientes. Sus indicaciones son múltiples: la Rx de tórax ante cualquier síntoma
              cardiorrespiratorio, la Rx simple de cualquier parte del cuerpo en pacientes que ha sufrido algún accidente, la Rx simple de abdomen ante molestias del aparato digestivo, la radiografía simple de cráneo en traumatismos craneoencefálicos,
              hipertensión intracraneal, y ciertos tipos de tumores, etc.</p>
            <center><img src="images/radiologia/rad2.jpg" /> <img src="images/radiologia/rad3.jpg" /> <img src="images/radiologia/rad4.jpg" /> </center>

            <p>La Fluoroscopía permite visualizar en un monitor las diferentes regiones del cuerpo.</p>

            <p>Estudios contrastados son en los que se administra un material generalmente radiopaco (bario, materiales iodados) ya sea por vía oral (faringograma, esofagograma, serie gastroduodenal, tránsito intestinal), rectal (colon por enema), venosa
              (venografía), arterial (arteriografía), articular (artrografía), cavidad uterina (histerosalpingografía), vías aéreas (broncografía), linfática (linfografía), conductos salivales (sialografía), conducto raquídeo (mielografía). </p>
            <center><img src="images/radiologia/rad5.jpg" /> <img src="images/radiologia/rad6.jpg" /></center>

            <p>Estudios invasivos son los que se efectúan generalmente a través de punciones con la administración de medios de contraste y con ayuda de la fluroscopía como son: arteriografías, colangiografías percutáneas, embolizaciones arteriales o venosas,
              colocaciones de prótesis vasculares o de vía biliar, toma de biopsias percutáneas.</p>

            <p>Mamografía es el estudio de elección para el estudio de la glándula mamaria en pacientes mayores de 35 años y se efectúa con equipos especiales para este fin con generadores de alta frecuencia, chasises y pantallas especiales.
            </p>
            <center><img src="images/radiologia/mam1.jpg" /></center>
            <p>Tomografía lineal es un estudio actualmente desplazado por tomografía computada (TC) y resonancia magnética (RM), en el cual se podía ver una zona pequeña del cuerpo.</p>
            <p>
              Tomografía computada se revisa ha detalle en una sección especial de este sitio Web.</p>
            <center><img src="images/radiologia/tomo1.jpg" /><img src="images/radiologia/tomo2.jpg" /></center>
            <p>
              Se debe tener la precaución de no exponer mujeres embarazadas a los "Rayos X" ya que estos pueden provocar malformaciones a los fetos. Así mismo todo paciente debe ser expuesto a la menor dosis posible de "Rayos X".
            </p>
            <p>
              Dr. Luis E. Méndez Casarón.

            </p>





          </div>


        </div>
      </div>


    </div>

  </section>



  <section class="complete-footer">
    <div class="bottom-footer">
      <div class="container">

        <div class="row">
          <!--Foot widget-->
          <div class="col-xs-12 col-sm-12 col-md-12 foot-widget-bottom">
            <p class="col-xs-12 col-md-5 no-pad">MAGEST Software 2015 | All Rights Reserved</p>
            <ul class="foot-menu col-xs-12 col-md-7 no-pad">

              <li><a href="contacto.php">Contacto</a></li>
            <li><a href="links_rad.php">Links radiológicos</a></li>
            <li><a  href="verimagenes.php">VerImagenes</a></li>
            <li><a href="publico_gral.php">Público en general</a></li>
            <li><a href="quienes_somos.php">¿Quiénes somos?</a></li>
            <li><a href="index.php">Inicio</a></li>


            </ul>
          </div>
        </div>
      </div>
    </div>

  </section>

  <!--JS Inclution-->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
  <script type="text/javascript" src="bootstrap-new/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
  <script type="text/javascript" src="js/jquery.scrollUp.min.js"></script>
  <script type="text/javascript" src="js/jquery.sticky.min.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/jquery.flexisel.min.js"></script>
  <script type="text/javascript" src="js/jquery.imedica.min.js"></script>
  <script type="text/javascript" src="js/custom-imedicajs.min.js"></script>

</body>

</html>
