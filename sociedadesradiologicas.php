<!DOCTYPE HTML>
<!--[if gt IE 8]> <html class="ie9" lang="en"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


  <title>SVRI</title>

  <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

  <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
  <link href="css/animate.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/green.css" id="style-switch" />

  <!-- REVOLUTION BANNER CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

  <!--[if IE 9]>
      <link rel="stylesheet" type="text/css" href="css/ie9.css" />
    <![endif]-->

  <link rel="icon" type="image/png" href="images/LOGO.svg">
  <link rel="stylesheet" type="text/css" href="css/inline.min.css" />
</head>

<body>

  <?php include 'menuPrincipal.html'; ?>

  <!--Mid Content Start-->


  <section class="complete-content content-footer-space">

    <div class="about-intro-wrap pull-left">

      <div class="bread-crumb-wrap ibc-wrap-1">
        <div class="container">
          <!--Title / Beadcrumb-->
          <div class="inner-page-title-wrap col-xs-12 col-md-12 col-sm-12">
            <div class="bread-heading">
              <h1>Sociedades radiológicas</h1></div>
            <div class="bread-crumb pull-right">
              <ul>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="links_rad.php">Links radiológicos</a></li>
                <li><a href="sociedadesradiologicas.php">Sociedades radiológicas</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="container">

        <!--Bottom Services-->
        <div class="services-bottom-wrap pull-left">
          <div class="container">

            <div class="row">

              <div class="col-md-12 col-xs-12 col-sm-12 pull-left full-content wow fadeInLeft animated" data-wow-delay="0.3s" data-wow-offset="130">
                <div class="full-content-title">Nacionales</div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.fmri.org.mx/"target="_blank"><img alt="" class="img-responsive" src="images/sociedades/fmri.jpg" /></a>
                  <div class="bottom-service-title">Federación Mexicana de Radiología e Imagen</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="http://www.smri.org.mx/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/smri.png" /></a>
                  <div class="bottom-service-title">Sociedad Mexicana de Radiología e Imagen</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.cotarai.org/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/ctri.jpg" /></a>
                  <div class="bottom-service-title">Colegio Tabasqueño de Radiología E Imagen A.C.
                  </div>

                </div>

                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.crinl.org.mx/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/crinl.png" /></a>
                  <div class="bottom-service-title">Colegio de Radiología E Imagen de Nuevo León</div>

                </div>


              </div>
              <!--- segunda fila-->

              <div class="row">

                <!--bottom-service-box-->


                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="wwww.svri.org.mx/svri/amim.html"><img alt="" class="img-responsive" src="images/sociedades/amim.jpg" /></a>
                  <div class="bottom-service-title">Asociación Mexicana de Imagenología Mamaria A.C.
                  </div>

                </div>
                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="http://www.cmri.org.mx/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/cmri.jpg" /></a>
                  <div class="bottom-service-title">Colegio Mexicano de Radiología e Imagen A.C.</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.criep.org/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/criep.jpg" /></a>
                  <div class="bottom-service-title">Colegio de Radiología e Imagen del Estado de Püebla. </div>

                </div>

                <!--bottom-service-box-->

              </div>






            </div>


            <div class="row">

              <div class="col-md-12 col-xs-12 col-sm-12 pull-left full-content wow fadeInLeft animated" data-wow-delay="0.3s" data-wow-offset="130">
                <div class="full-content-title">Extrajeras</div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.webcir.org/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/cir.jpg" /></a>
                  <div class="bottom-service-title">Colegio Interamericano de Radiología</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="http://www.rsna.org/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/rsna.jpg" /></a>
                  <div class="bottom-service-title">Radiological Society of North America Inc. </div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="seram.es" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/seram.jpg" /></a>
                  <div class="bottom-service-title">Sociedad Española de Radiología Médica</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2.5s" data-wow-offset="200">
                  <a href="http://www.myesr.org/cms/website.php?id=/en/ESR_ECR_news.htm" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/esr.jpg" /></a>
                  <div class="bottom-service-title">European Society of Radiology
                  </div>

                </div>

              </div>
              <!--- segunda fila-->

              <div class="row">

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.sar.org.ar/web/index.php" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/sar.jpg" /></a>
                  <div class="bottom-service-title">Sociedad Argentina de radiología</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="http://www.slarp.net/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/slarp.jpg" /></a>
                  <div class="bottom-service-title">Sociedad Latinoamericana de Radiología Pediátrica</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://cbr.org.br/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/cbr.jpg" /></a>
                  <div class="bottom-service-title">Colegio Brasileiro de Radiología</div>

                </div>

                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.sochradi.cl/web/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/scr.jpg" /></a>
                  <div class="bottom-service-title">Sociedad Chilena de Radiología</div>

                </div>


                <!--bottom-service-box-->

              </div>


              <div class="row">

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.acronline.org/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/acr.gif" /></a>
                  <div class="bottom-service-title">Asociación Colombiana de Radiología</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="http://www.car.ca/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/car.jpg" /></a>
                  <div class="bottom-service-title">Canadian Association of Radiologists </div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.sru.org/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/sru.jpg" /></a>
                  <div class="bottom-service-title">Society of radiologists in ultrasound </div>

                </div>

                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.ismrm.org/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/ismrm.jpg" /></a>
                  <div class="bottom-service-title">International Society for Magnetic Resonance in Medicine</div>

                </div>


                <!--bottom-service-box-->

              </div>

              <div class="row">

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.acr.org/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/acr.png" /></a>
                  <div class="bottom-service-title">American College of Radiology</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="http://www.isradiology.org/isr/index.php" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/isr.jpg" /></a>
                  <div class="bottom-service-title">International Society of Radiology</div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.essr.org/cms/website.php?id=/en/essr_home.htm" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/essr.gif" /></a>
                  <div class="bottom-service-title">European Society of Musculoskeletal Radiology</div>

                </div>

                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="https://skeletalrad.org/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/ssr.png" /></a>
                  <div class="bottom-service-title">Society of Skeletal Radiology</div>

                </div>


                <!--bottom-service-box-->

              </div>

              <div class="row">

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.musoc2015.in/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/musoc.jpg" /></a>
                  <div class="bottom-service-title">Musculoskeletal Society</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="http://www.sirweb.org/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/sir.jpg" /></a>
                  <div class="bottom-service-title">Society of Interventional Radiology
                  </div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.pedrad.org/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/pedrad.jpg" /></a>
                  <div class="bottom-service-title">The Society of Pediatric Radiology</div>

                </div>

                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.soveradi.com.ve/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/soveradi.gif" /></a>
                  <div class="bottom-service-title">Sociedad Venezolana de Radiología </div>

                </div>


                <!--bottom-service-box-->

              </div>




              <div class="row">

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.brighamandwomens.org/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/bwh.jpg" /></a>
                  <div class="bottom-service-title">Brigham and Women's Hospital</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="http://www.sociedadbolivianaderadiologia.com/sbr/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/sbr.jpg" /></a>
                  <div class="bottom-service-title">Sociedad Boliviana de Radiología
                  </div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.sedim.es/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/sedim.jpg" /></a>
                  <div class="bottom-service-title">Sociedad Española de Diagnostico por Imagen de la Mama</div>

                </div>

                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.sriuy.org.uy/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/sriuy.png" /></a>
                  <div class="bottom-service-title">Soc. de Radiología e Imagenología del Uruguay.
                  </div>

                </div>


                <!--bottom-service-box-->

              </div>



              <div class="row">

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="200">
                  <a href="http://www.sopari.org/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/sopari.jpg" /></a> 
                  <div class="bottom-service-title">Sociedad Panameña de Radiología e Imagen</div>

                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="1.5s" data-wow-offset="200">
                  <a href="http://www.pediatricradiology.com/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/pr.jpg" /></a>
                  <div class="bottom-service-title">Pediatic Radiology
                  </div>
                </div>

                <!--bottom-service-box-->
                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.seri.org.ec/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/seri.png" /></a>
                  <div class="bottom-service-title">Sociedad Ecuatoriana de Radiología</div>

                </div>

                <div class="col-xs-12 col-md-3 col-sm-6 col-lg-3 bottom-service-box no-pad wow fadeInDown animated" data-wow-delay="2s" data-wow-offset="200">
                  <a href="http://www.asoradgua.com.gt/" target="_blank"><img alt="" class="img-responsive" src="images/sociedades/asoradgu.jpg" /></a>
                  <div class="bottom-service-title">Asociación Civil de Diagnóstico por Imágenes y Terapia Radiante de Guatemala
                  </div>

                </div>


                <!--bottom-service-box-->

              </div>






            </div>


          </div>



        </div>
      </div>

    </div>








  </section>



  <!--Mid Content End-->


  <section class="complete-footer">
    <div class="bottom-footer">
      <div class="container">

        <div class="row">
          <!--Foot widget-->
          <div class="col-xs-12 col-sm-12 col-md-12 foot-widget-bottom">
            <p class="col-xs-12 col-md-5 no-pad">MAGEST Software 2015 | All Rights Reserved </p>
            <ul class="foot-menu col-xs-12 col-md-7 no-pad">

              <li><a href="contacto.php">Contacto</a></li>
            <li><a href="links_rad.php">Links radiológicos</a></li>
            <li><a  href="verimagenes.php">VerImagenes</a></li>
            <li><a href="publico_gral.php">Público en general</a></li>
            <li><a href="quienes_somos.php">¿Quiénes somos?</a></li>
            <li><a href="index.php">Inicio</a></li>


            </ul>
          </div>
        </div>
      </div>
    </div>

  </section>


  <!--JS Inclution-->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
  <script type="text/javascript" src="bootstrap-new/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
  <script type="text/javascript" src="js/jquery.scrollUp.min.js"></script>
  <script type="text/javascript" src="js/jquery.sticky.min.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/jquery.flexisel.min.js"></script>
  <script type="text/javascript" src="js/jquery.imedica.min.js"></script>
  <script type="text/javascript" src="js/custom-imedicajs.min.js"></script>

</body>

</html>
