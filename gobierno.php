<!DOCTYPE HTML>
<!--[if gt IE 8]> <html class="ie9" lang="en"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


  <title>SVRI</title>

  <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
  <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
  <link href="css/animate.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/green.css" id="style-switch" />

  <!-- REVOLUTION BANNER CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

  <!--[if IE 9]>
      <link rel="stylesheet" type="text/css" href="css/ie9.css" />
    <![endif]-->

  <link rel="icon" type="image/png" href="images/LOGO.svg">
  <link rel="stylesheet" type="text/css" href="css/inline.min.css" />
</head>

<body>

  <?php include 'menuPrincipal.html'; ?>

  <section class="complete-content content-footer-space">

    <!--Mid Content Start-->


    <div class="about-intro-wrap pull-left">

      <div class="bread-crumb-wrap ibc-wrap-5">
        <div class="container">
          <!--Title / Beadcrumb-->
          <div class="inner-page-title-wrap col-xs-12 col-md-12 col-sm-12">
            <div class="bread-heading">
              <h1>Gobierno</h1></div>
            <div class="bread-crumb pull-right">
              <ul>
                <li><a href="index.html">Inicio</a></li>
                <li><a href="quienes_somos.html">¿Quiénes somos?</a></li>
                <li><a href="gobierno.html">Gobierno</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="container">

        <div class="row">
          <!--About-us top-content-->






          <div class="col-xs-12 col-sm-12 col-md-12 pull-left doctors-3col-tabs no-pad">


            <div class="content-tabs tabs col-xs-12 col-sm-12">

              <!-- Tab panes -->
              <div class="tab-content">

                <div class="tab-pane fade fade-slow in active" id="all-doc">

                  <!--Doc intro-->
                  <div class="doctor-box col-md-4 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-delay="0.5s" data-wow-offset="200">

                    <div class="zoom-wrap">
                      <div class="zoom-icon"></div>
                      <img alt="" class="img-responsive" src="images/gobierno/6.png" />
                    </div>
                    <div class="doc-name">
                      <div class="doc-name-class">Dr. Alberto Cuevas Trujillo</div><span class="doc-title">Presidente</span>
                    </div>
                  </div>

                  <!--Doc intro-->
                  <div class="doctor-box col-md-4 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-delay="0.5s" data-wow-offset="200">

                    <div class="zoom-wrap">
                      <div class="zoom-icon"></div>
                      <img alt="" class="img-responsive" src="images/gobierno/Castillo_Segura_Carmen.jpg" />
                    </div>
                    <div class="doc-name">
                      <div class="doc-name-class">Dra. Carmen Elena Castillo Segura</div><span class="doc-title">Vicepresidente</span>
                    </div>
                  </div>

                  <!--Doc intro-->
                  <div class="doctor-box col-md-4 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-delay="0.5s" data-wow-offset="200">

                    <div class="zoom-wrap">
                      <div class="zoom-icon"></div>
                      <img alt="" class="img-responsive" id="bigger-image" src="images/gobierno/2.png" />
                    </div>
                    <div class="doc-name">
                      <div class="doc-name-class">Dr. Luis Eduardo Méndez Casarin</div><span class="doc-title">Secretario</span>
                    </div>
                  </div>

                  <!--Doc intro-->
                  <div class="doctor-box col-md-4 col-sm-6 col-xs-12 wow fadeInDown animated" data-wow-delay="0.5s" data-wow-offset="200">

                    <div class="zoom-wrap">
                      <div class="zoom-icon"></div>
                      <img alt="" class="img-responsive" src="images/gobierno/3.png" />
                    </div>
                    <div class="doc-name">
                      <div class="doc-name-class">Dr. Adrián López Contreras</div><span class="doc-title">Tesorero</span>
                    </div>
                  </div>

                  <!--Doc intro-->
                  <div class="doctor-box col-md-4 col-sm-6 col-xs-12 wow fadeInDown animated" data-wow-delay="0.5s" data-wow-offset="200">

                    <div class="zoom-wrap">
                      <div class="zoom-icon"></div>
                      <img alt="" class="img-responsive" id="smaller-image" src="images/gobierno/1.png" />
                    </div>
                    <div class="doc-name">
                      <div class="doc-name-class">Dr. Julián Sánchez Cortázar</div><span class="doc-title">Asesor General</span>
                    </div>
                  </div>

                  <!--Doc intro-->
                  <div class="doctor-box col-md-4 col-sm-6 col-xs-12 wow fadeInDown animated" data-wow-delay="0.5s" data-wow-offset="200">

                    <div class="zoom-wrap">
                      <div class="zoom-icon"></div>
                      <img alt="" class="img-responsive" src="images/gobierno/4.png" />
                    </div>
                    <div class="doc-name">
                      <div class="doc-name-class">Dr. Jorge Herrera Cantillo</div><span class="doc-title">Coordinador General</span>
                    </div>
                  </div>

                  <!--Doc intro-->
                  <div class="doctor-box col-md-4 col-sm-6 col-xs-12 wow fadeInUp animated" data-wow-delay="0.5s" data-wow-offset="200">

                    <div class="zoom-wrap">
                      <div class="zoom-icon"></div>
                      <img alt="" class="img-responsive" src="images/gobierno/5.png" />
                    </div>
                    <div class="doc-name">
                      <div class="doc-name-class">Dr. Federico Grosskelwing Acosta</div><span class="doc-title">Asesor General Adjunto</span>
                    </div>
                  </div>


                </div>


              </div>

            </div>
            <!--Mid Services End-->

          </div>

        </div>
      </div>

      <!--Mid Content End-->

    </div>

    <!--Footer Start-->

  </section>

  <section class="complete-footer">

    <div class="bottom-footer">
      <div class="container">

        <div class="row">
          <!--Foot widget-->
          <div class="col-xs-12 col-sm-12 col-md-12 foot-widget-bottom">
            <p class="col-xs-12 col-md-5 no-pad">MAGEST Software 2015 | All Rights Reserved</p>
            <ul class="foot-menu col-xs-12 col-md-7 no-pad">

              <li><a href="contacto.html">Contacto</a></li>
              <li><a href="links_rad.html">Links radiológicos</a></li>
              <li><a  href="verimagenes.html">VerImagenes</a></li>
              <li><a href="publico_gral.html">Público en general</a></li>
              <li><a href="quienes_somos.html">¿Quiénes somos?</a></li>
              <li><a href="index.html">Inicio</a></li>


            </ul>
          </div>
        </div>
      </div>
    </div>

  </section>


  <!--JS Inclution-->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
  <script type="text/javascript" src="bootstrap-new/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
  <script type="text/javascript" src="js/jquery.scrollUp.min.js"></script>
  <script type="text/javascript" src="js/jquery.sticky.min.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/jquery.flexisel.min.js"></script>
  <script type="text/javascript" src="js/jquery.imedica.min.js"></script>
  <script type="text/javascript" src="js/custom-imedicajs.min.js"></script>

</body>

</html>
