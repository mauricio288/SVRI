
<!DOCTYPE HTML>
<!--[if gt IE 8]> <html class="ie9" lang="en"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


    <title>SVRI</title>

    <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
    <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <link href="css/animate.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/green.css" id="style-switch" />

    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

    <!--[if IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie9.css" />
    <![endif]-->

    <link rel="icon" type="image/png" href="images/LOGO.svg">
    <link rel="stylesheet" type="text/css" href="css/inline.min.css" />
	    <script src='https://www.google.com/recaptcha/api.js'></script>




	<script language="JavaScript">

			function MM_findObj(n, d) {//v4.01
				var p, i, x;
				if (!d)
					d = document;
				if (( p = n.indexOf("?")) > 0 && parent.frames.length) {
					d = parent.frames[n.substring(p + 1)].document;
					n = n.substring(0, p);
				}
				if (!( x = d[n]) && d.all)
					x = d.all[n];
				for ( i = 0; !x && i < d.forms.length; i++)
					x = d.forms[i][n];
				for ( i = 0; !x && d.layers && i < d.layers.length; i++)
					x = MM_findObj(n, d.layers[i].document);
				if (!x && d.getElementById)
					x = d.getElementById(n);
				return x;
			}

			function MM_validateForm() {//v4.0
				var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
				for ( i = 0; i < (args.length - 2); i += 3) {
					test = args[i + 2];
					val = MM_findObj(args[i]);
					if (val) {
						nm = val.name;
						if (( val = val.value) != "") {
							if (test.indexOf('isEmail') != -1) {
								p = val.indexOf('@');
								if (p < 1 || p == (val.length - 1))
									errors += '- ' + nm + ' debe contener una direccion de correo electronico.\n';
							} else if (test != 'R') {
								num = parseFloat(val);
								if (isNaN(val))
									errors += '- ' + nm + ' debe contener un numero.\n';
								if (test.indexOf('inRange') != -1) {
									p = test.indexOf(':');
									min = test.substring(8, p);
									max = test.substring(p + 1);
									if (num < min || max < num)
										//errors += '- ' + nm + ' debe contener un numero entre ' + min + ' y ' + max + '.\n';
										errors += '- ' + nm + ' debe contener 10 digitos.\n';
								}
							}
						} else if (test.charAt(0) == 'R')
							errors += '- ' + nm + ' es necesario.\n';
					}
				}
				if (errors)
					alert('Los siguientes errores han ocurrido:\n' + errors);
				document.MM_returnValue = (errors == '');
			}


		</script>
		<script language="JavaScript" src="mm_menu.js"></script>
		<script type="text/javascript" src="../panel/js/jquery-1.7.2.js"></script>


		</head>

<body>


	<?php include 'menuPrincipal.html'; ?>

    <section class="complete-content content-footer-space">


      <div class="about-intro-wrap pull-left">

     <div class="bread-crumb-wrap ibc-wrap-1">
        <div class="container">
    <!--Title / Beadcrumb-->
            <div class="inner-page-title-wrap col-xs-12 col-md-12 col-sm-12">
                <div class="bread-heading"><h1>Iniciar sesión</h1></div>
                <div class="bread-crumb pull-right">
                <ul>
                <li><a href="index.html">Inicio</a></li>
                <li><a href="bienvenida.php">Iniciar sesión</a></li>
                </ul>
                </div>
            </div>
         </div>
     </div>

         <div class="container">

            <div class="row">

            <!--About-us top-content-->

            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 column-element">
				<script type="text/javascript">

					$(document).ready(function(){
						$("#nav").each(function(event){
							$(this).on('click','a',function(event){
								event.preventDefault();
								var href = $(this).attr("href");
								$("#contenido").load(href);

							});
						});
					});
          		</script>

		<div id="nav">
		<center>

		<h1>Inicio de sesión proximamente</h1>

		<!-- <a href="formularioLogin.php"><button class="btn btn-success btn-lg">Iniciar sesión</button></a> -->
        <!-- <a href="solicitudIngreso.php"><button class="btn btn-success btn-lg">Registrarse</button></a> -->

		</center>
		<br>
		<br>
	   
    </div>

<div id="contenido">
    <center>
      <h2>Calendario de sesiones ordinarias 2017</h2><br>
      <div style="font-size:22px;">
        28 ENERO<BR><BR>
        18 MARZO<BR><BR>
        30 ABRIL<br><br>
        15 JULIO<br><br>
        14 OCTUBRE<br><br>
        16 DICIEMBRE<br><br>
      </div>

      <h2>Curso SMRI 15 al 18 de Febrero<br>
      XXI VER-IMÁGENES <BR>
      28 ABRIL AL 1º DE MAYO <BR><br>
      XXV AÑOS DE S.V.R.I</h2>
      <h3>AGOSTO-SEPTIEMBRE</h3>

    </center>
</div>
    




         </div>
      </div>


    </div>

   </section>



    <section class="complete-footer">

    <div class="bottom-footer">
    <div class="container">

        <div class="row">
            <!--Foot widget-->
            <div class="col-xs-12 col-sm-12 col-md-12 foot-widget-bottom">
            <p class="col-xs-12 col-md-5 no-pad">MAGEST Software 2015 | All Rights Reserved</p>
            <ul class="foot-menu col-xs-12 col-md-7 no-pad">

            <li><a href="contacto.html">Contacto</a></li>
            <li><a href="links_rad.html">Links radiológicos</a></li>
            <li><a  href="verimagenes.html">VerImagenes</a></li>
            <li><a href="publico_gral.html">Público en general</a></li>
            <li><a href="quienes_somos.html">¿Quiénes somos?</a></li>
            <li><a href="index.html">Inicio</a></li>



            </ul>
            </div>
        </div>
    </div>
    </div>

    </section>

    <!--JS Inclution-->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="bootstrap-new/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="js/jquery.scrollUp.min.js"></script>
    <script type="text/javascript" src="js/jquery.sticky.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/jquery.flexisel.min.js"></script>
    <script type="text/javascript" src="js/jquery.imedica.min.js"></script>
    <script type="text/javascript" src="js/custom-imedicajs.min.js"></script>

</body>
</html>
