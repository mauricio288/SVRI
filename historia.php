<!DOCTYPE HTML>
<!--[if gt IE 8]> <html class="ie9" lang="en"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


  <title>SVRI</title>

  <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
  <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
  <link href="css/animate.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/green.css" id="style-switch" />

  <!-- REVOLUTION BANNER CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

  <!--[if IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie9.css" />
    <![endif]-->

  <link rel="icon" type="image/png" href="images/LOGO.svg">
  <link rel="stylesheet" type="text/css" href="css/inline.min.css" />
</head>

<body>

  <?php include 'menuPrincipal.html'; ?>

  <section class="complete-content content-footer-space">

    <div class="about-intro-wrap pull-left">

      <div class="bread-crumb-wrap ibc-wrap-1">
        <div class="container">
          <!--Title / Beadcrumb-->
          <div class="inner-page-title-wrap col-xs-12 col-md-12 col-sm-12">
            <div class="bread-heading">
              <h1>Historia</h1></div>
            <div class="bread-crumb pull-right">
              <ul>
                <li><a href="index.html">Inicio</a></li>
                <li><a href="quienes_somos.html">¿Quiénes somos?</a></li>
                <li><a href="historia.html">Historia</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="container">

        <div class="row">

          <!--About-us top-content-->

          <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 column-element">

            <h3>Historia </h3>
            <p>
              El primer antecedente fue el 13 de Enero de 1979, cuando se fundó la <b>Asociación Veracruzana de Radiólogos del Estado de Veracruz.</b>
            </p>
            <p>
              El Dr. Rigoberto Rodríguez de la Torre fue su Presidente, siendo avalado, como testigos, por el Dr. Manuel Cardoso Ramón, Presidente
              de la Federación Mexicana de Sociedades de Radiología y el Dr. Adán Pitol Croda, Presidente del Consejo Mexicano
              de Radiodiagnóstico y Radioterapia. </p>

            <p>
              Esta Asociación duró varios años y fungieron también como Presidentes el Dr. Alfredo Sánchez Fuentes y Dr. Santiago Schleske Ruiz.
              El 14 de Agosto de 1992 se funda la Sociedad Veracruzana de Radiología e Imagen, A.C.  La Mesa Directiva estuvo integrada por el Dr. Alfredo Sánchez Fuentes:
              Presidente. Dr. Francisco Arredondo Herrera: Vice-Presidente. Dr. Jorge Herrera Cantillo: Secretario y Dr. Santiago Schleske Ruiz: Tesorero.
            </p>
            <p>
              Los Socios Fundadores fueron: Dr. Juan Francisco Arredondo Herrera, Dr. Carlos Francisco Casián Salas, Dr. Federico Grosskelwing Acosta, Dr. Jorge Herrera Cantillo, Dr. Jorge Herrera López de Llergo, Dr. Aucencio Adrián López Contreras, Dr. Daniel Montes
              García, Dr. José Luis Rivera Calderón, Dr. Rigoberto Rodríguez de la Torre, Dr. Alfredo Sánchez Fuentes, Dr. Santiago Schleske Ruiz, Dr. Jaime Simonín Cruz, Dr. Sergio Simpson Morando, Dra. Elena Vázquez Luna, Dr. David Torio Bátiz y Dr.
              Jorge Viveros Parker. Durante la gestión de esta Directiva, se logró con el apoyo del Dr. Julián Sánchez Cortázar, que el XXV Curso Internacional de Ultrasonido, que realiza la Federación Mexicana de Radiología e Imagen, A.C. que estaba
              Presidida por el Dr. Miguel Stoopen Rometti, se efectuara por primera vez en el Puerto de Veracruz.
            </p>
            <p>
              En Julio del año de 1995 toma posesión la Directiva encabezada por el Dr. Jorge Herrera Cantillo, Secretario el Dr. Federico Grosskelwing Acosta y Tesorero el Dr. Jorge Viveros Parker. Fue Reelecta en 1997 y se incorpora como Vice-Presidente el Dr. Leopoldo
              Enrique Rodríguez Montiel. La Sociedad Veracruzana de Radiología e Imagen, A.C. participa en los Festejos del Centenario para conmemorar el descubrimiento de los Rayos X. Por invitación del Consejo Mexicano de Radiología e Imagen, A.C. participa
              como observador en el Examen de Certificación, siendo la única Sociedad Federada que asiste para tal fin. Participa activamente en el desarrollo del XVI Curso Internacional de Ultrasonido, que por segunda vez, en forma consecutiva se realizó
              en el Puerto de Veracruz.
            </p>

            <p>
              La Sociedad Veracruzana de Radiología e Imagen, A.C. participa en los Festejos del Centenario para conmemorar el descubrimiento de los Rayos X.
            </p>
            <p>
              Por invitación del Consejo Mexicano de Radiología e Imagen, A.C. participa como observador en el Examen de Certificación,
              siendo la única Sociedad Federada que asiste para tal fin.
            </p>
            <p>
              Participa activamente en el desarrollo del XVI Curso Internacional de Ultrasonido, que por segunda vez, en forma consecutiva se realizó en el Puerto de Veracruz.
            </p>
            <p>
              En el año de 1996, durante el desarrollo del IX Congreso Nacional de Radiología, efectuado en Can-Cún, Q.R., se acordó que la Sociedad Veracruzana de Radiología e Imagen, en el mes de Mayo de cada año efectuara el Curso VER-IMAGENES, con la participación
              del Colegio Interamericano de Radiología y con el Auspicio de la Federación Mexicana de Radiología e Imagen, A.C., y avalado por la Universidad Veracruzana y el Consejo Mexicano de Radiología e Imagen, A.C. </p>
            <p>
              La Sociedad Veracruzana de Radiología e Imagen, A.C., ha realizado el Curso Internacional VER-IMAGENES en 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007 y 2008 con la participación de Profesores extranjeros miembros del C.I.R.:
            </p>
            <div class="col-md-4 col-sm-4 col-lg-4 col-xs-4 column-element">
              <ul>
                <li>Dr. José Carlos Ugarte Suárez (Cuba)</li>
                <li>Dr. David Feigin (USA)</li>
                <li>Dr. Enrique Jiménez Quezada (Nicaragua)</li>
                <li>Dr. Francisco Arredondo Mendoza (Guatemala)</li>
                <li>Dr. Francisco Mirambell Solís, (Costa Rica)</li>
                <li>Dr. Franklin Mónico Portillo, (El Salvador)</li>
                <li>Dr. Diego Núñez, (USA)</li>
                <li>Dra. Janneth Bu Figueroa, (Honduras)</li>
                <li>Dr. César S. Pedrosa, (España)</li>
                <li>Dr. Francisco Quiroz y Ferrari (USA)</li>
                <li>Dr. Enrique Palacios Maryorga (USA)</li>
                <li>Dr. Jorge Banasco Domínguez (Cuba)</li>
                <li>Dr. Oswaldo Ramos Núñez (Venezuela)</li>
                <li>Dr. Luis Apesteguía Ciriza (España)</li>
                <li>Dr. Pedro Unshelm Báez (Venezuela)</li>
                <li>Dr. Luis Quevedo Sotolongo (Cuba)</li>
                <li>Dr. Iván Pedrosa Moral, (USA / España)</li>
                <li>Dr. José Luis Del Cura Rodríguez (España)</li>
                <br>
              </ul>
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4 col-xs-4 column-element">
              <ul>
                <li>Dra. Epifanía Luzardo Núñez (Cuba)</li>
                <li>Dr. Adolfo Maldonado Sánchez (USA)</li>
                <li>Dr. Pedro Guembe Urtiaga (España)</li>
                <li>Dr. Carlos R. Giménez Morales (Argentina)</li>
                <li>Dr. Marcelino Iribar de Marcos (España)</li>
                <li>Dr. Luiz Karpovas (Brazil)</li>
                <li>Dr. Antonio Vega Bolivar (España)</li>
                <li>Dr. Vicente Martínez de Vega (España)</li>
                <li>Dr. José Antonio López Ruiz (España)</li>
                <li>Dr. Helios Calvo Yeisi (Paraguay)</li>
                <li>Dr. Javier Casillas del Moral (USA)</li>
                <li>Dr. Juan Horta Simón (Cuba)</li>
                <li>Dr. Rafael Salvador Tarrazón (España)</li>
                <li>Dr. Alberto Marangoni (Argentina)</li>
                <li>Dr. Marvin Gutiérrez Sánchez (Nicaragua)</li>
                <li>Dr. Ricardo Dómina Gemma (Argentina)</li>
                <li>Dr. Carlos A. Bruguera (Argentina)</li>
                <li>Dra. María Lucrecia Ballarino (Argentina)</li>
                <li></li>
                <br>
              </ul>
            </div>
            <div class="col-md-4 col-sm-4 col-lg-4 col-xs-4 column-element">
              <ul>
                <li>Dr. Luis Chavarría Estrada (Costa Rica)</li>
                <li>Dr. Armando Zamora Ruiz (Guatemala)</li>
                <li>Dr. César Gotta (Argentina)</li>
                <li>Dr. Javier Casillas Del Moral (USA)</li>
                <li>Dr. Daniel Hierro García (Cuba)</li>
                <li>Dr. Rodrigo Restrepo (Colombia)</li>
                <li>Dra. María de los Angeles Figueroa García (El Salvador)</li>
                <li>Dr. Jean Francois Moruea (Francia)</li>
                <li>Dr. José Lipsich (Argentina)</li>
                <li>Dr. Alejandro Rasumoff (Argentina)</li>
                <li>Dr. Osvaldo Velán (Argentina)</li>
                <li>Dra. Ilka Guerrero Avendaño (Panamá)</li>
                <li>Dr. Alfredo Buzzi (Argentina)</li>
                <li>Dr. Gerant Rivera Sanfeliz (Puerto Rico)</li>
                <li>Dra. María Julia Valdés Estrada (Cuba)</li>
                <li>Dr. Pablo Ross (USA)</li>
                <li>Dr. Luis Valle Garrido (Cuba)</li>
                <li>Dra. Lina Marcela Cadavid Alvarez (Colombia)</li>
                <li>Dr. Claude Manelfe</li>
              </ul>
            </div>

            <p>
              Los Profesores nacionales han sido Presidentes y Expresidentes de la Federación y Consejo Mexicanos de Radiología e Imagen, A.C.:
            </p>
            <p>
              <ul>
                <li>Dr. José de Jesús de la Torre Beltrán</li>
                <li>Dr. Francisco Avelar Garnica</li>
                <li>Dr. Miguel Stoopen Rometti</li>
                <li>Dr. Ramón Ponte Romero</li>
                <li>Dr. Raúl Takenaga Mesquida</li>
                <li>Dr. Ramiro Johnson Vela</li>
                <li>Dr. Guillermo Elizondo Riojas</li>
                <li>Dr. Julián Sánchez Cortázar quien fue nombrado en 1997 Socio Honorario y a partir de 1998, la Conferencia Inaugural del Curso VER-IMAGENES, lleva su Nombre.</li>

              </ul>
            </p>


            <p>Sociedad Mexicana de Radiología e Imagen, A.C.:
              <div class="col-md-4 col-sm-4 col-lg-4 col-xs-4 column-element">
                <ul>
                  <li>Dr. Ernesto J. Dena Espinosa</li>
                  <li>Dr. Rafael Rojas Jasso</li>
                  <li>Dr. José Antonio Pérez Mendizábal</li>
                  <li>Dra. Elia García Torres</li>
                  <li>Dra. Janet Tanus Hajj</li>
                  <li>Dr. Carlos Muñoz Rivera</li>
                  <li>Dra. Paulina Bezaury Rivas</li>
                  <li>Dr. Oscar Quiroz Castro</li>
                  <li>Dra. Yolanda Villaseñor Navarro</li>
              </div>
              <div class="col-md-4 col-sm-4 col-lg-4 col-xs-4 column-element">
                <li>Dra. Velia Rubio Gutiérrez</li>
                <li>Dr. José Luis Criales Cortés</li>
                <li>Dr. Kenji Kimura Fujikami</li>
                <li>Dra. Guadalupe Guerrero Avendaño</li>
                <li>Dra. Margarita Fuentes García</li>
                <li>Dr. Gonzalo Salgado Salgado</li>
                <li>Dr. Héctor Murrieta González</li>
                <li>Dr. Carlos Sartorius Rodríguez</li>
                <li>Dra. Aida Pérez Lara</li>
              </div>
              <div class="col-md-4 col-sm-4 col-lg-4 col-xs-4 column-element">
                <li>Dr. Luis Ramos Durán</li>
                <li>Dr. Raúl Barreda Escalante</li>
                <li>Dra. Victoria Falcón Solís</li>
                <li>Dr. Marcos Tawil Facovich</li>
                <li>Dra. Veronique Borois Boullar</li>
                <li>Dr. Enrique Mainero Crespo</li>
                <li>Dr. Jaime Saavedra Abril</li>
                <li>Dr. Marco Antonio Téliz Menéses</li>
                <li>Dr. José Luis Ramírez Arías</li>
                </ul>
              </div>
            </p>
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-6 column-element">


              <p>Sociedad Regiomontana de Radiología e Imagen, A.C.:
                <ul>
                  <li>Dr. Antonio Guardiola Fernández</li>
                  <li>Dr. Felipe Garza Gracia</li>
                  <li>Dra. Margarita Garza Montemayor</li>
                  <li>Dr. Guillermo Elizondo Riojas</li>
                </ul>
              </p>
              <p>

                Sociedad de Radiología e Imagen de la Laguna, A.C.:
                <ul>
                  <li>Dr. Manuel Olhagaray Rivera, Dr. Javier Aguilar Chávez.</li>
                </ul>
              </p>
              <p>
                Colegio de Radiología, Ultrasonido e Imagen del Estado de Puebla A.C.:
                <ul>
                  <li>Dr. José Aguilar Guerrero</li>
                  <li>Dr. Héctor Pablo Labastida Ocampo</li>
                </ul>
              </p>
              <p>

                Sociedad Mexicana de Neuro-Radiología Diagnóstica y Terapéutica, A.C.
                <ul>
                  <li>Dr. Roque Ferreyro Irigoyen</li>
                  <li>Dr. Jorge Balderrama Bañares</li>
                  <li>Dr. Jesús Taboada Barajas</li>
                </ul>
              </p>
              <p> Sociedad Tabasqueña de Radiología e Imagen, A.C.
                <ul>
                  <li>Dr. Angel Niño de Rivera Hermosillo</li>
                  <li>Dra. Matilde Hernández Trejo</li>
                </ul>
              </p>
              <p>
                Sociedad de Radiología e Imagen del Sureste, A.C.
                <ul>
                  <li>Dra. Mildred Riera Palma</li>
                  <li>Dr. Ricardo Palma Capetillo</li>
                  <li>Dr. Edgardo Martínez Menéndez</li>
                </ul>
              </p>
              <p>
                Sociedad Chihuahuense de Radiología e Imagen, A.C.
                <ul>
                  <li>Dr. Guillermo Federico Muller García</li>
                </ul>
              </p>
              <p>
                Sociedad de Radiología e Imagen de Coahuila, A.C.:
                <ul>
                  <li>Dr. Oscar Valdés Dávila </li>
                </ul>
              </p>
              <p>
                Colegio de Radiología e Imagen de Jalisco, A.C.:
                <ul>
                  <li>Dra. Beatriz Eugenia González Ulloa</li>
                  <li>Dr. Sergio Peregrina González</li>
                </ul>
              </p>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6 col-xs-6 column-element">
              <p>
                Sociedad de Radiología de Baja California, A. C.:
                <ul>
                  <li>Dr. Xavier de la Barrera Figueroa</li>
                </ul>

              </p>


              <p>
                Colegio de Radiología e Imágen del Centro, A.C.:
                <ul>
                  <li>Dr. Alberto Sahagún Jiménez</li>
                  <li>Dr. Miguel Ángel Arceo García</li>
                  <li>Dr. Raúl Ibarra Gil</li>
                  <li>Dr. Eduardo Sampson Zaldivar</li>
                </ul>
              </p>
              <p>
                Sociedad Veracruzana de Radiología e Imagen, A.C.
                <ul>
                  <li>Dr. César Víctor Manuel Pérez Cortés</li>
                  <li>Dr. Leopoldo E. Rodríguez Montiel</li>
                  <li>Dr. Juan Cervantes Monroy</li>
                  <li>Dr. Rafael Hernández González</li>
                  <li>Dr. Raúl Herrera Cantillo</li>
                  <li>Dr. Federico Grosskelwing Acosta</li>
                  <li>Dr. Armando Páez Rocha</li>
                  <li>Dr. Alfredo Rivera Secchi</li>
                  <li>Dra. Minerva Pérez Pérez</li>
                  <li>Dr. Leobardo Justo García Prieto</li>
                  <li>Dr. Alberto Cuevas Trujillo</li>
                  <li>Dr. David Fortino Mercado García</li>
                  <li>Dr. Enrique Castán Lugo</li>
                  <li>Dra. Martha Luz Schetino Ochoa</li>
                  <li>Dr. Carlos González Pimentel</li>
                  <li>Dra. Fanny Viveros Pasquel</li>
                  <li>Dra. Amparo Malfavón Malpica</li>
                  <li>Dra. Rubí Espejo Fonseca</li>
                  <li>Dra. Carmen Elena Castillo Segura</li>
                  <li>Dr. Jorge Viveros Parker</li>
                  <li>P.T.R. Armando Edgar Carral Carrasco</li>
                  <li>Dr. Arturo Albrandt Salmerón</li>
                  <li>Dr. Jorge Herrera Cantillo</li>
                  <li>Dr. Antonio René Pérez López</li>
                  <li>Dra. María Luisa de la Cruz Arías</li>
                  <li>Dr. Ramiro Malagrini</li>
                </ul>
              </p>
            </div>
            <p>La participación entusiasta de todos los integrantes de la Sociedad en la organización y coordinación de los eventos, hicieron posible que en el año del 2003 se lograra que DOCE Sociedades Federadas estuvieran representadas por sus Presidentes
              o Socios Distinguidos: además de la F.M.R.I.; C.M.R.I. y Sociedad Veracruzana de Radiología e Imagen, A.C.; lo que convirtió al Curso Internacional VER-IMAGENES; EN EL FORO ACADÉMICO REGIONAL CON LA MAYOR REPRESENTACIÓN DE LA RADIOLOGÍA
              MEXICANA. </p>
            <p>Además la Sociedad ha organizado con la Universidad Veracruzana, en la Ciudad de Xalapa, Ver., cursos anuales desde 1993 al 2000, dirigidos principalmente a la comunidad estudiantil de esa Universidad. A partir del año 1998 el Curso ha contado
              con la participación de Profesorado Internacional y el Aval del Colegio Interamericano de Radiología. </p>
            <p>En 1997 se realizó JOVENRAD 97 con la participación del Distinguido Maestro y amigo: Dr. Rogelio Moncada. Desde 1997 se han realizado en Veracruz, exámenes de Certificación por el Consejo Mexicano de Radiología e Imagen, que ha permitido que
              el 92% de los Socios que integran nuestra Sociedad estén Certificados por dicho Consejo. La Sociedad ha recopilado los exámenes desde 1994 a 1999 y con el espíritu que impera en la Federación, los ha enviado a las Sociedades que los han
              solicitado, para que sirvan de guía a los médicos que presentarán el mencionado examen. </p>
            <p>Nuestra Sociedad ha obtenido el Primer Lugar Nacional en los exámenes realizados en Agosto de 1997 y 1998. Siendo el Dr. Juan Cervantes Monroy y el Dr. Leobardo Justo García Prieto, respectivamente, los que obtuvieron esta distinción. En año
              2000 tres residentes miembros de la Sociedad, estuvieron dentro de los 10 primeros lugares. </p>
            <p>Es la única Sociedad que ha aportado al Fondo de Educación de la Federación, un donativo, por las utilidades obtenidas en un Curso. La Sociedad Veracruzana de Radiología e Imagen, y la Sociedad Cubana de Radiología, realizaron del 13 al 17
              de Septiembre de 1999, en la Habana, Cuba, con el Auspicio del Colegio Interamericano de Radiología y la Federación Mexicana de Radiología e Imagen, A.C., con el Aval de la Universidad Veracruzana y del Consejo Mexicano de Radiología e Imagen,
              A.C., VER-IMÁGENES EN CUBA, siendo la primera Sociedad Federada que logra la Internacionalización, con lo cual contribuyó a dar mayor realce a los eventos para Conmemorar el XXV Aniversario de la Fundación de la Federación Mexicana de Radiología
              e Imagen, A.C. </p>
            <p>En Mayo del 2000 toma posesión la Directiva que Preside el Dr. Jorge Viveros Parker; Vice-Presidente: Dr. Alejandro Schleske Ruíz; Secretario: Dr. Luis Eduardo Méndez Casarín; y Tesorero: Dr. Sergio Simpson Morando. Reelecta para el periodo
              2002-2004 En el mes de Septiembre del 2001 en el Hospital Clínico Universitario de San Carlos, se llevó a cabo el Curso VER-IMAGENES EN MADRID, con la Coordinación del Dr. César S. Pedrosa. El Curso contó con el Aval de la Sociedad Española
              de Radiología Médica, (SERAM), el Colegio Interamericano de Radiología (CIR), y con el reconocimiento de la Universidad Complutense de Madrid. Siendo el primer curso organizado por una Sociedad Federada, en otro Continente. </p>
            <p>En el año de 2001 la Sociedad Veracruzana de Radiología e Imagen, instituyó un reconocimiento como Socio Honorífico, a personajes que han participado en el desarrollo tecnológico y comercial de la industria radiológica mexicana. Estableciendo
              con esto, una distinción sin precedente en nuestro País. </p>
            <p>Desde el año 2002, la Sociedad Veracruzana de Radiología e Imagen, ha contado con el apoyo de la Sociedad Española de Imagen Mamaria (SEDIM), suscribiendo ambas agrupaciones un convenio de cooperación académica. Lo que permitió que desde esta
              fecha, VER-IMAGENES sea un Curso Internacional Iberoamericano. </p>
            <p>De manera inininterrupida la Sociedad Veracruzana de Radiología e Imagen, A.C., ha asistido al Cambio de Mesa Directiva de la Sociedad Mexicana de Radiología e Imagen, A.C. desde 2003 hasta 2008. I.M.V.I.T.A. 2003Imagenología Mamaria Ver Imágenes
              Taller de Actualización. Primer Curso Taller Interactivo en Patología Mamaria en la República Mexicana. La Sociedad Veracruzana de Radiología e Imagen, es pionera en el Turismo-Académico con intercambio científico-cultural en Egipto en Septiembre
              de 2003. Presencia en Africa de la Radiología Mexicana. En Mayo del 2004 toma posesión la Directiva que Preside el Dr. Alejandro Schleske Ruíz; Vice-Presidente: Dra. Minerva Pérez Pérez: Secretario Dr. Luis Eduardo Méndez Casarín; y Tesorero:
              Dr. Sergio Simpson Morando. De nueva cuenta la Habana es sede en Septiembre de 2004, de VER-IMAGENES EN CUBA, con la participación por primera vez en la Isla del Dr. César Pedrosa, invitado por nuestra Sociedad y la Sociedad Cubana de Radiología.
              En el mes de Febrero de 2005. Por la decisión tomada por la Federación Mexicana de Radiología e Imagen, A.C. de que México dejara de pertenecer al Colegio Interamericano de Radiología, los integrantes de la Sociedad Veracruzana de Radiología
              e Imagen, A.C. por acuerdo unánime en Asamblea General, determinaron su desafiliación a la Federación Mexicana de Radiología e Imagen, A.C., y la totalidad de su membresía solicitó seguir perteneciendo al CIR, lo cual es un hecho sin precedente
              en nuestro País y en el Seno del Colegio. En Julio de 2005, la Sociedad Veracruzana de Radiología e Imagen, A.C., ampliando sus relaciones internacionales, logró que 16 compañeros de Nicaragua y uno más de Belice, se afiliaran a nuestra
              Sociedad, con lo que la integración con Centroamérica es una realidad. La relación fraterna con la Sociedad Argentina de Radiología, hizo posible que del 9 al 11 de Septiembre de 2005, en Buenos Aires, la Sociedad Veracruzana de Radiología
              e Imagen, participara en el 51° Congreso Argentino de Radiología, para celebrar el 1er. Encuentro Veracruzano Argentino V.I.B.A. 2005 VER-IMÁGENES EN BUENOS AIRES. Participaron como Profesores: Dra. Rubi Espejo Fonseca, Dra. Margarita Garza
              Montemayor, Dr. Jorge Herrera Cantillo. Donde se firmó un convenio de cooperación académica, que permitió que en el X Curso Internacional Iberoamericano VER-IMÁGENES 2006, participaran distinguidos Profesores Argentinos miembros de la Sociedad
              Argentina de Radiología y de la Federación Argentina de Asociaciones de Radiología, Diagnóstico por Imágenes y Terapia Radiante. La Sociedad Veracruzana de Radiología e Imagen, A.C. ha enviado desde 2006 a la fecha, Profesores al Congreso
              Argentino de Radiología: 2006: Dr. Carlos Rodríguez Treviño; 2007: Dr. Jaime Saavedra Abril; 2008: Dr. Kenji Kimura Fujikami. En Mayo del 2006 toma posesión la Directiva que por primera vez es Presidida por una mujer, Dra. Minerva Pérez
              Pérez; Vice-Presidente: Dr. Alberto Cuevas Trujillo; Secretario: Dr. Luis Eduardo Méndez Casarín; y Tesorero: Dr. Aucencio Adrián López Contreras. En el mes de Febrero se firman los Convenios de Colaboración Científica, Académica y Cultural
              con la Sociedad Mexicana de Radiología e Imagen, A.C. En Septiembre con la Sociedad Paraguaya de Radiología e Imagen y con la Sociedad Venezolana de Radiología y Diagnóstico por Imágenes. En Noviembre con la Sociedad Nicaragüense de Radiología
              e Imágenes. Por invitación expresa de la International Society of Radiology y del Colegio Interamericano de Radiología, la Sociedad Veracruzana de Radiología e Imagen, A.C. organizó V I S A VER-IMÁGENES EN SUDAFRICA en el marco del 24° Congreso
              Internacional de Radiología que se efectuó en Ciudad del Cabo del 12 al 16 de Septiembre de 2006. VER-IMÁGENES 2007, ofreció como innovación, el Curso-Taller de Densitometría Osea. Método diagnóstico, que también es territorio de nuestra
              Especialidad y que cambió la dinámica de muchos otros Cursos y Congresos. El Turismo-Académico modalidad creada por nuestra Sociedad, permitió organizar en Septiembre VIMOS 2007 VER-IMÁGENES EN MOSCU, primera experiencia mexicana en Europa
              del Este. El 6 de Diciembre, la Sociedad Veracruzana de Radiología e Imagen, A.C., asiste al cambio de Mesa Directiva de la Asociación Salvadoreña de Radiología, Ultrasonido e Imágenes Diagnósticas y se firma un Convenio de Colaboración
              Científica, Académica y Cultural entre nuestras agrupaciones. La Sociedad Veracruzana de Radiología e Imagen y su Curso VER-IMAGENES2008, se engalanaron con la asistencia del Dr. Claude Manelfe, Presidente de la International Society of
              Radiology y del Dr. Ricardo García Mónaco, Presidente del Colegio Interamericano de Radiología. VER-IMAGENES 2008, contó además con la participación del Ex Presidente de la I.S.R. Dr. Francisco Arredondo Mendoza y todos los Ex Presidentes
              del C.I.R. desde 1994 a 2004. Dr. José Luis Ramírez Arías, Dr. Diego Nuñez, Dr. Francisco Arredondo Mendoza, Dr. Carlos Giménez Morales, Dr. Miguel Stoopen Rometti. Lo cual no tiene precedente en el ámbito Científico Internacional. En Mayo
              del 2008 toma posesión la Directiva Presidida por el Dr. Alberto Cuevas Trujillo; Vice-Presidente: Dra. Carmen Elena Castillo Segura; Secretario: Dr. Luis Eduardo Méndez Casarín; y Tesorero: Dr. Aucencio Adrián López Contreras. Participamos
              en el 25° Congreso Internacional de Radiología efectuado en Marrakech, Marruecos del 5 al 8 de Junio. VIEM 2008 VER-IMAGENES EN MARRUECOS. Iniciamos la planeación para asistir al 26° Congreso Internacional de Radiología que se realizará
              en Shangai, China en Abril del 2010. Septiembre de 2008, se firma el Convenio de Colaboración Científica, Académica y Cultural con la Sociedad Panameña de Radiología e Imagen. La Sociedad Veracruzana de Radiología e Imagen,A.C., está integrada
              por 137 miembros, 30 de los cuales han merecido ser nombrados Socios Honorarios.

            </p>

          </div>
        </div>


      </div>
    </div>


    </div>

  </section>



  <section class="complete-footer">

    <div class="bottom-footer">
      <div class="container">

        <div class="row">
          <!--Foot widget-->
          <div class="col-xs-12 col-sm-12 col-md-12 foot-widget-bottom">
            <p class="col-xs-12 col-md-5 no-pad">MAGEST Software 2015 | All Rights Reserved</p>
            <ul class="foot-menu col-xs-12 col-md-7 no-pad">

              <li><a href="contacto.php">Contacto</a></li>
            <li><a href="links_rad.php">Links radiológicos</a></li>
            <li><a  href="verimagenes.php">VerImagenes</a></li>
            <li><a href="publico_gral.php">Público en general</a></li>
            <li><a href="quienes_somos.php">¿Quiénes somos?</a></li>
            <li><a href="index.php">Inicio</a></li>



            </ul>
          </div>
        </div>
      </div>
    </div>

  </section>

  <!--JS Inclution-->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
  <script type="text/javascript" src="bootstrap-new/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
  <script type="text/javascript" src="js/jquery.scrollUp.min.js"></script>
  <script type="text/javascript" src="js/jquery.sticky.min.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/jquery.flexisel.min.js"></script>
  <script type="text/javascript" src="js/jquery.imedica.min.js"></script>
  <script type="text/javascript" src="js/custom-imedicajs.min.js"></script>

</body>

</html>
