<!DOCTYPE HTML>
<!--[if gt IE 8]> <html class="ie9" lang="en"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


  <title>SVRI</title>


  <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
  <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
  <link href="css/animate.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/green.css" id="style-switch" />

  <!-- REVOLUTION BANNER CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

  <!--[if IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie9.css" />
    <![endif]-->

  <link rel="icon" type="image/png" href="images/LOGO.svg">

  <link rel="stylesheet" type="text/css" href="css/inline.min.css" />
</head>

<body>

<?php include 'menuPrincipal.html'; ?>

  <section class="complete-content">

    <!--Mid Content Start-->


    <div class="about-intro-wrap pull-left">

      <div class="bread-crumb-wrap ibc-wrap-6">
        <div class="container">
          <!--Title / Beadcrumb-->
          <div class="inner-page-title-wrap col-xs-12 col-md-12 col-sm-12">
            <div class="bread-heading">
              <h1> Gabinetes radiológicos del Estado de Veracruz</h1></div>
            <div class="bread-crumb pull-right">
              <ul>
                <li><a href="index.html">Inicio</a></li>
                <li><a href="publico_gral.html">Público en general</a></li>
                <li><a href="gabinetes.html">Gabinetes radiológicos</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="container">


        <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 column-element">

          Aquí se encuentran los enlaces para los gabinetes radiológicos atendidos por médicos radiólogos avalados por:
          <ul>
            <li>Consejo Mexicano de Radiología A.C.</li>
            <li>Federación Mexicana de Radiología e Imagen A.C.</li>
            <li>Sociedad Veracruzana de Radiología e Imagen A.C.</li>
          </ul>
        </div>



        <!--About-us top-content-->

        <div class="col-xs-12 col-sm-12 col-md-12 pull-left Testiminal-page-wrap no-pad wow fadeInUp animated" data-wow-delay="0.5s" data-wow-offset="200">


          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 test-box">
            <p><b>Córdoba , Ciudad de los 30 Caballeros</b>
              <br> Diagnóstico Médico Alameda.
              <br> Atendido por los Drs. Federico Grosskelwing A. y Carlos González Pimentel
              <br> Calle 2 No. 1113 y 1117 Tel. 01.271.7140424, 7146808
              <br> Fracc. Alameda, Córdoba Ver.
              <br>
              <a href="http://www.diagnosticomedicoalameda.com" target="_blank">Página web</a>
            </p>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 test-box">

            <p>
              <b>Coatzacoalcos</b>
              <br> Clínica de Diagnóstico Radiológico e Imagen "Dr. Jorge Herrera"
              <br> Hidalgo # 401 Centro Coatzacoalcos, Ver. C.P. 96400
              <br> Tel. y Fax: (9) 21-2-28-83, 21-2-16-41, 21-2-16-42
              <br> Urgencias: (9) 21-2-04-15, 21-5-00-88, Cel: (044) 927-3-15-14; (01) 5-406-88-86
              <br> E-mail: herrerarx@hotmail.com
            </p>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 test-box">

            <p>
              <b>Huatusco</b>
              <br> Dr. Luis E. Mé Casarin
              <br> Dirección: Avenida 4 Oriente No 501 Colonia centro.
              <br> Huatusco, Ver. C.P. 94100
              <br> Tel. (2)7340174 Domicilio (2)7341568
              <br> E-mail: luimenc@uolmail.com.mx
              <br>
              <br>
              <br>
            </p>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 test-box">
            <p>
              <b>Xalapa de Enriquez </b>
              <br> LABORATORIO ALFA DE XALAPA S.C.
              <br> AV. Manuel ï¿½vila Camacho 42 Local 13 AL 16
              <br> CP. 91000 COL. Centro Xalapa,Ver.
              <br> TEL. 01 22 88185158 Y TEL.FAX 01 2288 185198
              <br> Atendido por el Dr. Aucencio Adrián López Contreras
              <br> E-mail: rayolab@svri.org.mx
              <br>
              <a href="#"></a>

            </p>
          </div>



          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 test-box">

            <p>
              <b>Orizaba </b>
              <br> GABINETE DE IMAGEN DIAGNOSTICO
              <br> Norte 12 No. 478 entre oriente 9 y 11 Orizaba Ver.
              <br> Tel. 726-65-00
              <br> Atendido por el Dr. Publio Saldaía Ruiz.
              <br> IMAGEN DIAGNÓSTICA.
              <br> Norte 6 No. 1124 entre Oriente 21 y 23 Orizaba Ver.
              <br> Tel 7261408
              <br> Atendido por la Dra. Sara Benítez Palafox.

            </p>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 test-box">
            <p>
              <b>Veracruz </b>
              <br> GABINETE RADIOLÓGICO DEL CENTRO MÉDICO LATINOAMERICANO
              <br> 16 de Septiembre No. 956 Veracruz Ver.
              <br> Tel. 9311932 y 9322849
              <br> Atendido por el Dr. Juan A. Fco. Cervantes Monroy.
              <br> RESONANCIA DE VERACRUZ.
              <br> 16 de Septiembre No. 956 Veracruz Ver.
              <br> Tel. 9311932 y 9322849
              <br> Atendido por el Dr. Alfredo Rivera Secchi.



            </p>
          </div>



          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 test-box">
            <p>
              <b>Cerro Azul </b>
              <br> RAYO X Y US DE LA HUASTECA.
              <br> Morelos s/n Col. campo 1er. de Mayo Cerro Azul Veracruz.
              <br> Tel. 0178585.
              <br> Atendido por la Dra. Minerva Pérez Pérez.

            </p>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 test-box">
          </div>




        </div>





      </div>
    </div>

    <div class="cl-wrap icl-wrap">
      <div class="container">

        <div class="row"style="display:none">

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 pull-left client-logo-flex wow flipInX" data-wow-delay="0.5s" data-wow-offset="100">

            <ul id="testi-client-logos" class="icl-carousel">
              <li>
                <a href="#"><img alt="" src="images/nlogo1.png" class="img-responsive client-logo-img" /></a>
              </li>
              <li>
                <a href="#"><img alt="" src="images/nlogo2.png" class="img-responsive client-logo-img" /></a>
              </li>
              <li>
                <a href="#"><img alt="" src="images/nlogo3.png" class="img-responsive client-logo-img" /></a>
              </li>
              <li>
                <a href="#"><img alt="" src="images/nlogo4.png" class="img-responsive client-logo-img" /></a>
              </li>
              <li>
                <a href="#"><img alt="" src="images/nlogo5.png" class="img-responsive client-logo-img" /></a>
              </li>
              <li>
                <a href="#"><img alt="" src="images/nlogo6.png" class="img-responsive client-logo-img" /></a>
              </li>
            </ul>

          </div>

        </div>

      </div>
    </div>

    <!--Mid Content End-->



    <!--Footer Start-->

  </section>


  <section class="complete-footer">

    <div class="bottom-footer">
      <div class="container">

        <div class="row">
          <!--Foot widget-->
          <div class="col-xs-12 col-sm-12 col-md-12 foot-widget-bottom">
            <p class="col-xs-12 col-md-5 no-pad">MAGEST Software 2015 | All Rights Reserved</p>
            <ul class="foot-menu col-xs-12 col-md-7 no-pad">

              <li><a href="contacto.php">Contacto</a></li>
            <li><a href="links_rad.php">Links radiológicos</a></li>
            <li><a  href="verimagenes.php">VerImagenes</a></li>
            <li><a href="publico_gral.php">Público en general</a></li>
            <li><a href="quienes_somos.php">¿Quiénes somos?</a></li>
            <li><a href="index.php">Inicio</a></li>


            </ul>
          </div>
        </div>
      </div>
    </div>

  </section>


  <!--JS Inclution-->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
  <script type="text/javascript" src="bootstrap-new/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
  <script type="text/javascript" src="js/jquery.scrollUp.min.js"></script>
  <script type="text/javascript" src="js/jquery.sticky.min.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/jquery.flexisel.min.js"></script>
  <script type="text/javascript" src="js/jquery.imedica.min.js"></script>
  <script type="text/javascript" src="js/custom-imedicajs.min.js"></script>

</body>

</html>
