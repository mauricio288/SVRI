DROP DATABASE IF EXISTS SVRI;
CREATE DATABASE SVRI
	DEFAULT CHARACTER SET utf8
	DEFAULT COLLATE utf8_general_ci;
USE SVRI;

CREATE TABLE users (
	user_id 		INT				NOT NULL AUTO_INCREMENT,
	user_name		VARCHAR(45)		NOT NULL,
	user_type_id    INT          	NOT NULL,
	user_pass		VARCHAR(255)	NOT NULL,
	user_email		VARCHAR(45)		NOT NULL,
	user_pay		INT 			NOT NULL,
	user_course		VARCHAR(45) 	NOT NULL,
	
	name			VARCHAR(45)		NOT NULL,
	first_last_name	VARCHAR(45)		NOT NULL,

	PRIMARY KEY(user_id)
);