<!DOCTYPE HTML>
<!--[if gt IE 8]> <html class="ie9" lang="en"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


  <title>SVRI</title>

  <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
  <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
  <link href="css/animate.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/green.css" id="style-switch" />

  <!-- REVOLUTION BANNER CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

  <!--[if IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie9.css" />
    <![endif]-->

  <link rel="icon" type="image/png" href="images/LOGO.svg">
  <link rel="stylesheet" type="text/css" href="css/inline.min.css" />
</head>

<body>

<?php include 'menuPrincipal.html'; ?>

  <section class="complete-content content-footer-space">






    <div class="about-intro-wrap pull-left">

      <div class="bread-crumb-wrap ibc-wrap-1">
        <div class="container">
          <!--Title / Beadcrumb-->
          <div class="inner-page-title-wrap col-xs-12 col-md-12 col-sm-12">
            <div class="bread-heading">
              <h1>Tomografía</h1></div>
            <div class="bread-crumb pull-right">
              <ul>
                <li><a href="index.html">Inicio</a></li>
                <li><a href="publico_gral.html">Público en general</a></li>
                <li><a href="tomografia.html">Tomografía</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="container">

        <div class="row">

          <!--About-us top-content-->

          <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 column-element">

            <h3>Tomografía computarizada</h3>
            <p>El físico inglés Godfrey N. Hounsfield explicó el principio de la tomografía computarizada en 1968 al publicar la patente de este equipo su principio básico consiste en una exposición de un haz colimado de Rayos X en sentido axial a el paciente
              registrado por un detector colocado más allá del mismo, repitiendo en múltiples ángulos de la circunferencia para lograr una reconstrucción espacial después de complicadas operaciones matemáticas solo posibles en un tiempo razonable con
              ayuda de la una computadora.
            </p>
            <p>
              Desde 1972 en el hospital Morley de Inglaterra se instaló el primer tomógrafo computado comercial, desde esa fecha hasta el momento la evolución tecnológica ha sido espectacular avanzando desde ese primero tomógrafo de un solo detector que obtenía cada
              uno de los cortes en 4 minutos, hasta la actualizada en donde en 2 minutos se explora la totalidad del cuerpo humano con infinidad de cortes.
            </p>
            <p>
              No importando la tecnología usada un tomógrafo computarizado esta compuesto básicamente en tres sistemas:
            </p>
            <h4>Sistema de recolección de datos</h4>
            <p>
              Comprende los dispositivos que exploran y recogen la información primaria, consta de un generador de alta tensión que suministra la energía necesaria para el tubo de rayos X que se encuentra oculto, junto con los detectores en un estativo llamado gantry
              que es la porción del equipo en donde entra y sale el paciente durante el estudio, una camilla empotrada al gantry en donde l paciente se recuesta durante la exploración.
            </p>
            <p>
              En cuanto se inicia el estudio un haz colimado de radiación se le envía al paciente pasa a trabes de este como en los rayos X convencionales y la radiación que atraviesa es captada por los detectores en sus diferentes perfiles de intensidad, estos detectores
              los cuales actualmente son cámaras de gas (xenón) presurizado, son transductores que cambian la información que se reciben en forma de fotones a una señal electrónica que envía al sistema de procesamiento de datos.
            </p>
            <h4>Sistema de procesamiento de datos</h4>
            <p>
              Está constituido por un preamplificador que amplifica la señal eléctrica enviada por los detectores, un convertidor analogicodigital que convierte esta señal en números los cuales son enviados a la computadora, esta almacena todos los datos para reconstruir
              una imagen en base a los cálculos efectuados de los valores recibidos, la imagen se reconstruye en base a no menos de 250,000 ecuaciones se configura en una cuadricula llamada matriz cuyas dimensiones son variables dependiendo de cada equipo
              inicialmente se realizaron con matrices de 80 x 80 (renglones por columnas) que constituían 6400 cuadros, cada cuadro es llamado píxel que representa a la vez un punto en el monitor, en la actualidad la mayoría de los equipos nacionales
              crean matrices de 512 x 512.
            </p>
            <h4>Sistema de visualización y archivo</h4>
            <p>
              Es manejado por su totalidad por la computadora la cual guarda su información en su disco duro y puede ser visualizada en el monitor de la misma o puede ser impresa en formatos especiales fotográficos.
            </p>
            <p>
              La imagen se forma gracias a que cada valor de atenuación tiene un tono de gris diferente en cada escala preestablecida. El valor de atenuación es la representación en una escala de la capacidad que tiene cada parte del sujeto a explorar para atenuar
              la radiación, se le conoce también como coeficiente de atenuación y en honor a su descubridor se les denomina unidades Hounsfield las cuales se abrevian UH y se representan en números enteros.
            </p>
            <p>
              El total de la escala es variable de acuerdo a cada equipo, sin embargo se acepta que en una escala que va del -1000 al 1000 el -1000 se representa con el aire y el 1000 el metal pasando por el 0 que es el agua. Toda esta escala se puede observar en un
              monitor otorgándole un tono de gris diferente, sin embargo el ojo humano solo puede distinguir 20 tonos de gris por lo que deberemos variar la escala para poder visualizar solo ciertas partes de la escala que nos sean de interés para poder
              observar estos tejidos situación sumamente importante ya que la calidad de la imagen no depende de los valores preestablecidos en un programa de computadora sino que depende de la adecuación que se haga a los ojos del examinador de la patología
              demostrada en el corte correspondiente a cada paciente.
            </p>
            <p>
              Dr. Carlos González Pimentel
            </p>





          </div>


        </div>
      </div>


    </div>

  </section>



  <section class="complete-footer">

    <div class="bottom-footer">
      <div class="container">

        <div class="row">
          <!--Foot widget-->
          <div class="col-xs-12 col-sm-12 col-md-12 foot-widget-bottom">
            <p class="col-xs-12 col-md-5 no-pad">MAGEST Software 2015 | All Rights Reserved</p>
            <ul class="foot-menu col-xs-12 col-md-7 no-pad">

             <li><a href="contacto.php">Contacto</a></li>
            <li><a href="links_rad.php">Links radiológicos</a></li>
            <li><a  href="verimagenes.php">VerImagenes</a></li>
            <li><a href="publico_gral.php">Público en general</a></li>
            <li><a href="quienes_somos.php">¿Quiénes somos?</a></li>
            <li><a href="index.php">Inicio</a></li>


            </ul>
          </div>
        </div>
      </div>
    </div>

  </section>

  <!--JS Inclution-->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
  <script type="text/javascript" src="bootstrap-new/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
  <script type="text/javascript" src="js/jquery.scrollUp.min.js"></script>
  <script type="text/javascript" src="js/jquery.sticky.min.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/jquery.flexisel.min.js"></script>
  <script type="text/javascript" src="js/jquery.imedica.min.js"></script>
  <script type="text/javascript" src="js/custom-imedicajs.min.js"></script>

</body>

</html>
