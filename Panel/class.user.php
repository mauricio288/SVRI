<?php

require_once('dbconfig.php');

class USER
{	

	private $conn;
	
	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }
	
	public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		
		return $stmt;
	}
	
	public function register($uname,$umail,$upass,$rname,$ulast_name)
	{
		try
		{
			$new_password = password_hash($upass, PASSWORD_DEFAULT);
			
			$stmt = $this->conn->prepare("INSERT INTO users(user_name,user_email,user_pass,name,first_last_name) 
		        VALUES(:uname, :umail, :upass, :rname, :ulast_name)");
												  
			$stmt->bindparam(":rname", $rname);
			$stmt->bindparam(":uname", $uname);
			$stmt->bindparam(":ulast_name", $ulast_name);
			$stmt->bindparam(":umail", $umail);
			$stmt->bindparam(":upass", $new_password);
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}

	public function resetPassword($upass,$umail)
	{
		$new_password = password_hash($upass, PASSWORD_DEFAULT);
			
		$stmt = $this->conn->prepare("UPDATE users SET user_pass=:upass WHERE user_email='$umail'");

		$stmt->bindparam(":upass", $new_password);

		$stmt->execute();

		return $stmt;
	}

	public function registerCourse($cname,$ndescription,$course_price,$course_begin_date){

		try
		{
			$stmt = $this->conn->prepare("INSERT INTO courses(course_name , course_description , course_price, course_begin_date) 
				VALUES (:ncourse, :ndescription, :course_price, :course_begin_date)");

			$stmt->bindparam(":ncourse", $cname);
			$stmt->bindparam(":ndescription", $ndescription);
			$stmt->bindparam(":course_price", $course_price);
			$stmt->bindparam(":course_begin_date", $course_begin_date);

			$stmt->execute();

			return $stmt;
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}


	public function applyFor($cname)
	{
		$stmt = $this->conn->prepare("INSERT INTO courses(course_name) VALUES (:cname)");

		$stmt->bindparam(":cname", $cname);
		$stmt->execute();

		return $stmt;
	}

	public function deleteCourse($cid){
		
		try {
			$stmt = $this->conn->prepare("DELETE FROM courses WHERE course_id='$cid' ");

			$stmt->bindparam(":course_id", $cid);

			$stmt->execute();
			
			return $stmt;

		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}
	
	
	public function doLogin($umail,$upass)
	{
		try
		{
			$stmt = $this->conn->prepare("SELECT user_id, user_name, user_email, user_pass FROM users WHERE user_email= :umail ");
			$stmt->execute(array(':umail'=>$umail));
			$userRow=$stmt->fetch(PDO::FETCH_ASSOC);

			if($stmt->rowCount() == 1)
			{
				if(password_verify($upass, $userRow['user_pass']))
				{
					
					$_SESSION['user_session'] = $userRow['user_id'];

					return true;
				}
				else
				{
					return false;
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function isAdmin($umail)
	{

		try {

			$stmt = $this->conn->prepare("SELECT user_type_id FROM users WHERE user_email= :umail");
			$stmt->execute(array(':umail'=>$umail));
			$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
			//Obtienes el tipo de usuario aqui 
			$userType = $userRow['user_type_id'];

			
			if ($userType == '1') {

				$_SESSION['isAdm'] = $userRow['user_type_id'];

				return true;
			}
			else{
				return false;
			}
			
		} catch (PDOException $e) {

			echo $e->getMessage();
		}
	}

	public function is_loggedin()
	{

		if(isset($_SESSION['user_session']))
		{
			return true;
		}
	}

	public function redirect($url)
	{
		header("Location: $url");
	}
	
	public function doLogout()
	{
		session_destroy();
		unset($_SESSION['user_session']);
		return true;
	}
}
?>