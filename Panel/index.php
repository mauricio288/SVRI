<?php
session_start();
require_once("class.user.php");
$login = new USER();

if($login->is_loggedin()!="")
{
	$login->redirect('access.php');
}

if(isset($_POST['btn-login']))
{
	$uname = strip_tags($_POST['txt_uname_email']);
	$umail = strip_tags($_POST['txt_uname_email']);
	$upass = strip_tags($_POST['txt_password']);
		
	if($login->doLogin($umail,$upass))
	{

        if ($login->isAdmin($umail)) {
            $login->redirect('Admin.php');
        }
        else{
		$login->redirect('index.php');
        }
	}
	else
	{
		$error = "Algún dato es incorrecto, por favor intenta nuevamente";
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SVRI login</title>
<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="../bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" type="text/css" href="../css/green.css" id="style-switch" />
<link rel="stylesheet" type="text/css" href="../css/inline.min.css" />

</head>
<body>

<div class="signin-form">

	<div class="container">
     
        <?php include 'menuPanel.html'; ?>

        <section class=" content-footer-space">

            <div class="about-intro-wrap pull-left">

                <div class="bread-crumb-wrap ibc-wrap-1">
                    <div class="container">
                        <div class="inner-page-title-wrap col-xs-12 col-md-12 col-sm-12">
                            <div class="bread-heading"><h1>Iniciar sesión</h1></div>
                        </div>
                     </div>
                 </div>
             </div>


           <form class="form-signin" method="post" id="login-form">
            
                <div id="error">
                <?php
        			if(isset($error))
        			{
        				?>
                        <div class="alert alert-danger">
                           <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?> !
                        </div>
                        <?php
        			}
        		?>
                </div>
            
                <div class="form-group">
                    <input type="text" class="form-control" name="txt_uname_email" placeholder="correo" 
                    oninvalid="this.setCustomValidity('Por favor, escribe tu usurio')"
                    oninput="setCustomValidity('')" required />
                    <span id="check-e"></span>
                </div>
            
                <div class="form-group">
                    <input type="password" class="form-control" name="txt_password" placeholder="contraseña"  
                        oninvalid="this.setCustomValidity('Por favor, escribe tu contraseña')"
                        oninput="setCustomValidity('')" required />
                </div>
           
         	     <hr/>
            
                <div class="form-group" id="btn-submit">
                    <button type="submit" name="btn-login" class="btn btn-default" >
                    	<i class="glyphicon glyphicon-log-in"></i> &nbsp; Iniciar sesión
                    </button>
                </div>  
          	     <br />
                <label>¿No tienes una cuenta?<a href="sign-up.php"> Registrate</a></label><br /><br>
                <label><a href="reset_password.php">¿Olvidaste tu contraseña?</a></label>
          </form>

        </div>
    
    </div>

    <div id="contenido" style="padding-bottom: 100px">
        <center>
          <h2>Calendario de sesiones ordinarias 2017</h2><br>
          <div style="font-size:22px;">
            28 ENERO<BR><BR>
            18 MARZO<BR><BR>
            30 ABRIL<br><br>
            15 JULIO<br><br>
            14 OCTUBRE<br><br>
            16 DICIEMBRE<br><br>
          </div>

          <h2>Curso SMRI 15 al 18 de Febrero<br>
          XXI VER-IMÁGENES <BR>
          28 ABRIL AL 1º DE MAYO <BR><br>
          XXV AÑOS DE S.V.R.I</h2>
          <h3>AGOSTO-SEPTIEMBRE</h3>

        </center>
    </div>

    <section class="complete-footer">

        <div class="bottom-footer">
            <div class="container">

                <div class="row">
                <!--Foot widget-->
                    <div class="col-xs-12 col-sm-12 col-md-12 foot-widget-bottom">
                        <p class="col-xs-12 col-md-5 no-pad">MAGEST Software 2015 | All Rights Reserved</p>
                        <ul class="foot-menu col-xs-12 col-md-7 no-pad">
                            <li><a href="contacto.html">Contacto</a></li>
                            <li><a href="links_rad.html">Links radiológicos</a></li>
                            <li><a  href="verimagenes.html">VerImagenes</a></li>
                            <li><a href="publico_gral.html">Público en general</a></li>
                            <li><a href="quienes_somos.html">¿Quiénes somos?</a></li>
                            <li><a href="index.html">Inicio</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </section>

</body>
</html>