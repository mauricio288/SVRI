<?php
require ('session_check.php');
require_once ('dbconfig.php');
$find = new USER();
$admin = new USER();

$stmt = $find->runQuery("SELECT name, user_name ,first_last_name, user_email , user_course , user_pay FROM users");
$stmt->execute();
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script src="../js/jquery.js"></script>
	<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="../bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
	<link rel="stylesheet" type="text/css" href="../css/green.css" id="style-switch" />
	<link rel="stylesheet" type="text/css" href="../css/inline.min.css" />


	<script src="https://use.fontawesome.com/51186fe0cc.js"></script>


	<title>Bienvenido - <?php print($userRow['name']); ?></title>
</head>

<body>

	<?php include 'menuLogin.php'; ?>

	<div class="container">
		<div class="row">
			<did class="col-md-12" align="center" style="padding-bottom: 10px">
				<h3>Bienvenido <?php echo($userRow['name'] . " " . $userRow['first_last_name']); ?></h3>
			</did>
			<table class="table table-hover table-responsive">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Correo registrado</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<?php 
							foreach($stmt->FetchAll() as $results) {
							echo '<td>' . $results['name'] . " " . $results['first_last_name'] . '</td>';
							echo '<td>' . $results['user_email'] . '</td>';
							echo '<td>
									<button class="btn btn-primary">
										<i class="fa fa-envelope-o" aria-hidden="true"></i>
									</button>
									<button class="btn btn-danger">
										<span class="glyphicon glyphicon-trash"></span>
									</button>
								</td>';
							}
						?>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<script src="../bootstrap/js/bootstrap.min.js"></script>

</body>
</html>