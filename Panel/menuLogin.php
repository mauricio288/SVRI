  <header>

    <!--Topbar-->
    <div class="topbar-info no-pad">
      <div class="container">
        <div class="social-wrap-head col-md-2 no-pad">
          <ul>
            <li><a href="https://www.facebook.com/SVRIMG"><i class="icon-facebook head-social-icon" id="face-head" data-original-title="" title=""></i></a> </li>

          </ul>
        </div>
        <div class="top-info-contact pull-right col-md-6">Acceder a <a href="https://webmail.svri.org.mx">email</a></div>
        <!-- <div class="top-info-contact pull-right col-md-6">Llámanos +52 921 212 8132 | contact@svri.com</div> -->
      </div>
    </div>
    <!--Topbar-info-close-->
    
    <div id="headerstic">

      <div class=" top-bar container">
        <div class="row">
          <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">

                <button type="button" class="navbar-toggle icon-list-ul" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                </button>
                <button type="button" class="navbar-toggle icon-rocket" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                  <span class="sr-only">Toggle navigation</span>
                </button>

                <!-- <a href="index.php">
                  <div class="logo"></div>
                </a> -->
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="../index.php">Inicio</a>

                  </li>

                  <li class="dropdown"><a href="quienes_somos.php">¿Quiénes somos?<b class="icon-angle-down"></b></a>
                    <ul class="dropdown-menu">
                      <li><a href="historia.php">Historia</a>
                      </li>
                      <li><a href="gobierno.php">Gobierno</a>
                      </li>
                      <li><a href="galeria.php">Galeria</a></li>
                      <li><a href="socios.php">Socios</a>

                    </ul>
                  </li>



                    <li class="dropdown"><a href="publico_gral.php">Público en general<b class="icon-angle-down"></b></a>
                      <ul class="dropdown-menu">
                        <li><a href="radiologia.php">Radiología</a></li>
                        <li><a href="ultrasonido.php">Ultrasonido</a></li>
                        <li><a href="tomografia.php">Tomografía</a></li>
                        <li><a href="resonancia.php">Resonancia Magnética</a></li>
                        <li><a href="gabinetes.php">Gabinetes en Veracruz</a></li>

                      </ul>
                    </li>

                    <li class="dropdown"><a href="verimagenes.php">VerImagenes</a>
                    </li>

                    <li class="dropdown"><a href="links_rad.php">Links radiológicos<b class="icon-angle-down"></b></a>
                      <ul class="dropdown-menu">
                        <li class="dropdown left-dropdown"><a href="sociedadesradiologicas.php">Sociedades radiológicas</a>
                        </li>
                        <li class="dropdown left-dropdown"><a href="links.php">Links de interés</a>
                        </li>
                      </ul>
                    </li>

                    <li class="dropdown"><a href="contacto.php"><i class="icon-file"></i>Contacto</a>
                    </li>

                    <li class="dropdown active"><a href="index.php"><span class="glyphicon glyphicon-user"></span> <?php print($userRow['name']); ?></a>
                      <ul class="dropdown-menu">
                        <li class="dropdown left-dropdown"><a href="logout.php?logout=true">Cerrar sesión</a>
                        </li>
                      </ul>
                    </li>

                </ul>
              </div>

            </div>
            <!-- /.container-fluid -->
          </nav>
        </div>
      </div>
      <!--Topbar End-->
    </div>
  </header>
